--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

with GNAT.Command_Line;

with SBS.Version;
with SBS.Mmap;
with SBS.Chainer;
with SBS.Crypto.Signature;

procedure SBS_Create
is
   use Ada.Strings.Unbounded;

   Invalid_Cmd_Line : exception;

   Config      : GNAT.Command_Line.Command_Line_Configuration;
   Path_Key    : Unbounded_String;
   Path_Input  : Unbounded_String;
   Path_Output : Unbounded_String;

   --  Parse command line arguments.
   procedure Parse_Argument (Switch, Value : String);

   -------------------------------------------------------------------------

   procedure Parse_Argument (Switch, Value : String)
   is
   begin
      if Switch = "-k" or else Switch = "--key" then
         Path_Key := To_Unbounded_String (Value);
      elsif Switch = "-i" or else Switch = "--input-file" then
         Path_Input := To_Unbounded_String (Value);
      elsif Switch = "-o" or else Switch = "--output-file" then
         Path_Output := To_Unbounded_String (Value);
      end if;
   end Parse_Argument;
begin
   GNAT.Command_Line.Set_Usage
     (Config => Config,
      Usage  => "-k <key> -f <filename>",
      Help   => "Signed Block Stream (SBS) creator tool ("
      & SBS.Version.Version_String & ")");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Switch      => "-h",
      Long_Switch => "--help",
      Help        => "Display usage and exit");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-k:",
      Long_Switch => "--key:",
      Help        => "Path to private key");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-i:",
      Long_Switch => "--input-file:",
      Help        => "File to process");
   GNAT.Command_Line.Define_Switch
     (Config      => Config,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-o:",
      Long_Switch => "--output-file:",
      Help        => "Path to output file");
   begin
      GNAT.Command_Line.Getopt (Config => Config);

      if Path_Input = Null_Unbounded_String then
         raise Invalid_Cmd_Line with "Path to input file missing";
      end if;

      if Path_Output = Null_Unbounded_String then
         raise Invalid_Cmd_Line with "No output path specified";
      end if;

      if Path_Key = Null_Unbounded_String then
         raise Invalid_Cmd_Line with "Path to private key missing";
      end if;

   exception
      when GNAT.Command_Line.Invalid_Switch =>
         --  Automatically prints output.
         raise GNAT.Command_Line.Exit_From_Command_Line;
      when GNAT.Command_Line.Invalid_Parameter =>
         raise Invalid_Cmd_Line with "Missing argument for option '-"
           & GNAT.Command_Line.Full_Switch & "'";
   end;

   GNAT.Command_Line.Free (Config => Config);

   SBS.Process (Input_File  => To_String (Path_Input),
                Key_Path    => To_String (Path_Key),
                Output_File => To_String (Path_Output));

exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : SBS.Mmap.Mmap_Error
      | SBS.Chainer.Chainer_Error
      | SBS.Processing_Error
      | SBS.Crypto.Signature.Sign_Error
      | SBS.Crypto.Signature.Sign_IO_Error
      | SBS.Crypto.Signature.Sign_Key_Error =>
      Ada.Text_IO.Put_Line ("Processing failed: "
                            & Ada.Exceptions.Exception_Message (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : Invalid_Cmd_Line =>
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (X => E));
      Ada.Text_IO.Put_Line ("Try """ & Ada.Command_Line.Command_Name &
                              " --help"" for more information.");
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : others =>
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end SBS_Create;
