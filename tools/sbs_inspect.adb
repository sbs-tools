--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with System.Storage_Elements;

with Ada.Text_IO.Text_Streams;
with Ada.Streams;
with Ada.Exceptions;
with Ada.Directories;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

with GNAT.Command_Line;

with Interfaces;

with SBS.Config;
with SBS.Version;
with SBS.Types;
with SBS.Mmap;
with SBS.Utils;
with SBS.Crypto.Digest;

procedure SBS_Inspect
is
   use Ada.Strings.Unbounded;

   Invalid_Cmd_Line : exception;
   Inspect_Error    : exception;

   Cmd_Conf    : GNAT.Command_Line.Command_Line_Configuration;
   Path_Stream : Unbounded_String;

   Verbose, Print_Sig, Dump_Data : aliased Boolean;

   --  Parse command line arguments.
   procedure Parse_Argument (Switch, Value : String);

   --  Check file size and map given path. Raises Inspect_Error if file size is
   --  too small for an SBS image.
   procedure Map
     (Path :     String;
      Size : out SBS.Types.File_Size;
      Addr : out System.Address);

   --  Check SBS header. Unmaps address and raises Inspect_Error if path is not
   --  an SBS image.
   procedure Check_Header
     (Addr : System.Address;
      Size : SBS.Types.File_Size);

   --  Inspect file given by path, if Dump is True the encapsulated data is
   --  printed to stdout.
   procedure Inspect
     (Path : String;
      Dump : Boolean);

   --  Print signature data and exit.
   procedure Print_Signature (Path : String);

   -------------------------------------------------------------------------

   procedure Check_Header
     (Addr : System.Address;
      Size : SBS.Types.File_Size)
   is
      use type Interfaces.Unsigned_32;

      Header : constant SBS.Types.Header_Type
        with
          Import,
          Address => Addr;
   begin
      if Header.Version_Magic /= SBS.Config.Version_Magic then
         SBS.Mmap.Munmap (Address => Addr,
                          Size    => Size);
         raise Inspect_Error with "File does not contain a signed block "
           & "stream";
      end if;
   end Check_Header;

   -------------------------------------------------------------------------

   procedure Inspect
     (Path : String;
      Dump : Boolean)
   is
      use SBS;

      Size : Types.File_Size;
      Addr : System.Address;
   begin
      Map (Path => Path,
           Size => Size,
           Addr => Addr);
      Check_Header (Addr => Addr,
                    Size => Size);

      declare
         use Interfaces;

         Header : constant Types.Header_Type
           with
             Import,
             Address => Addr;
      begin
         if not Dump then
            Ada.Text_IO.Put_Line ("SBS file detected");
            Ada.Text_IO.Put_Line ("Block count         :"
                                  & Header.Block_Count'Img);
            Ada.Text_IO.Put_Line ("Block size          :"
                                  & Header.Block_Size'Img);
            Ada.Text_IO.Put_Line ("Block data length   :"
                                  & Unsigned_32'Image
                                    (Header.Block_Size
                                     - Unsigned_32 (Header.Hashsum_Length)));
            Ada.Text_IO.Put_Line ("Signature length    :"
                                  & Header.Signature_Data_Length'Img);
            Ada.Text_IO.Put_Line ("Header size         :"
                                  & Header.Header_Size'Img);
            Ada.Text_IO.Put_Line ("Hashsum length      :"
                                  & Header.Hashsum_Length'Img);
            Ada.Text_IO.Put_Line ("Padding length      :"
                                  & Header.Padding_Length'Img);
            Ada.Text_IO.Put_Line ("Hash algorithm ID 1 : "
                                  & Config.Hash_Algorithm_Type'Enum_Val
                                    (Header.Hash_Algorithm_ID_1)'Img);
            Ada.Text_IO.Put_Line ("Hash algorithm ID 2 : "
                                  & Config.Hash_Algorithm_Type'Enum_Val
                                    (Header.Hash_Algorithm_ID_2)'Img);
            Ada.Text_IO.Put_Line ("Hash algorithm ID 3 : "
                                  & Config.Hash_Algorithm_Type'Enum_Val
                                    (Header.Hash_Algorithm_ID_3)'Img);
            Ada.Text_IO.Put_Line ("Hash algorithm ID 4 : "
                                  & Config.Hash_Algorithm_Type'Enum_Val
                                    (Header.Hash_Algorithm_ID_4)'Img);
            Ada.Text_IO.Put_Line ("Signature scheme    : "
                                  & Config.Signature_Scheme_Type'Enum_Val
                                    (Header.Signature_Scheme_ID)'Img);
            Ada.Text_IO.Put_Line ("Root hash           : "
                                  & Utils.To_Hex_Str (A => Header.Root_Hash));

            Ada.Text_IO.New_Line;
            Ada.Text_IO.Put_Line ("Checking hashes ...");
         end if;

         declare
            use System.Storage_Elements;
            use Ada.Streams;

            Blocks : constant Types.Block_Array
              (1 .. Positive (Header.Block_Count))
                with
                  Import,
                  Address => Addr + Storage_Offset
                    (Config.Header_Size + Header.Signature_Data_Length);

            Next_Hash : Types.Hashsum_Type := Header.Root_Hash;
         begin
            for I in Blocks'Range loop
               declare
                  First : Ada.Streams.Stream_Element_Offset
                    := Blocks (I).Data'First;
                  H     : constant Types.Hashsum_Type
                    := Crypto.Digest.Hash (Block => Blocks (I));
               begin
                  if H /= Next_Hash then
                     Ada.Text_IO.Put_Line ("ERROR");
                     Ada.Text_IO.Put_Line
                       ("Computed hash "
                        & Utils.To_Hex_Str (A => H));
                     Ada.Text_IO.Put_Line
                       ("Stored hash   "
                        & Utils.To_Hex_Str (A => Next_Hash));
                     raise Inspect_Error with "Hash chain broken at block"
                       & I'Img;
                  else
                     if not Dump and Verbose then
                        Ada.Text_IO.Put (Utils.To_Hex_Str (A => Next_Hash));
                        Ada.Text_IO.Put_Line (" OK" & I'Img);
                     end if;
                     if Dump then
                        if I = Positive (Types.Block_Data_Range'First)
                          and Header.Padding_Length /= 0
                        then
                           First := Blocks (I).Data'First +
                             Stream_Element_Offset (Header.Padding_Length);
                        end if;
                        Types.Block_Data_Type'Write
                          (Ada.Text_IO.Text_Streams.Stream
                            (Ada.Text_IO.Standard_Output), Blocks (I).Data
                              (First .. Blocks (I).Data'Last));
                     end if;
                  end if;
               end;
               Next_Hash := Blocks (I).Hash_Of_Next_Block;
            end loop;

            if not Dump then
               Ada.Text_IO.Put_Line ("Hashes valid");
            end if;
         end;
      end;

      Mmap.Munmap (Address => Addr,
                   Size    => Size);
   end Inspect;

   -------------------------------------------------------------------------

   procedure Map
     (Path :     String;
      Size : out SBS.Types.File_Size;
      Addr : out System.Address)
   is
      use type SBS.Types.File_Size;
   begin
      Size := Ada.Directories.Size (Name => Path);

      if Size < SBS.Config.Header_Size + SBS.Config.Block_Size then
         raise Inspect_Error with "File '" & Path
           & "' is not a signed block stream - too small";
      end if;

      SBS.Mmap.Mmap_File (Filename => Path,
                          Size     => Size,
                          Mode     => SBS.Mmap.In_File,
                          Address  => Addr);
   end Map;

   -------------------------------------------------------------------------

   procedure Parse_Argument (Switch, Value : String)
   is
   begin
      if Switch = "-i" or else Switch = "--input-file" then
         Path_Stream := To_Unbounded_String (Value);
      end if;
   end Parse_Argument;

   -------------------------------------------------------------------------

   procedure Print_Signature (Path : String)
   is
      Size : SBS.Types.File_Size;
      Addr : System.Address;
   begin
      Map (Path => Path,
           Size => Size,
           Addr => Addr);
      Check_Header (Addr => Addr,
                    Size => Size);

      declare
         use System.Storage_Elements;
         use Ada.Streams;

         Header : constant SBS.Types.Header_Type
           with
             Import,
             Address => Addr;

         type Signature_Type is new Stream_Element_Array
            (1 .. Stream_Element_Offset (Header.Signature_Data_Length));

         Signature : constant Signature_Type
         with
            Import,
            Address => Addr + Storage_Offset (SBS.Config.Header_Size);
      begin
         Signature_Type'Write (Ada.Text_IO.Text_Streams.Stream
           (Ada.Text_IO.Standard_Output), Signature);
      end;
   end Print_Signature;

begin
   GNAT.Command_Line.Set_Usage
     (Config => Cmd_Conf,
      Usage  => "[-v] -i <signed block stream>",
      Help   => "Signed Block Stream (SBS) inspector tool ("
      & SBS.Version.Version_String & ")");
   GNAT.Command_Line.Define_Switch
     (Config      => Cmd_Conf,
      Switch      => "-h",
      Long_Switch => "--help",
      Help        => "Display usage and exit");
   GNAT.Command_Line.Define_Switch
     (Config      => Cmd_Conf,
      Callback    => Parse_Argument'Unrestricted_Access,
      Switch      => "-i:",
      Long_Switch => "--input-file:",
      Help        => "File to process");
   GNAT.Command_Line.Define_Switch
     (Config      => Cmd_Conf,
      Output      => Verbose'Access,
      Switch      => "-v",
      Long_Switch => "--verbose",
      Help        => "Enable verbose output");
   GNAT.Command_Line.Define_Switch
     (Config      => Cmd_Conf,
      Output      => Print_Sig'Unrestricted_Access,
      Switch      => "-p",
      Long_Switch => "--print-signature",
      Help        => "Print signature data");
   GNAT.Command_Line.Define_Switch
     (Config      => Cmd_Conf,
      Output      => Dump_Data'Unrestricted_Access,
      Switch      => "-x",
      Long_Switch => "--extract-data",
      Help        => "Extract payload data");
   begin
      GNAT.Command_Line.Getopt (Config => Cmd_Conf);

      if Path_Stream = Null_Unbounded_String then
         raise Invalid_Cmd_Line with "Path to stream file missing";
      end if;

   exception
      when GNAT.Command_Line.Invalid_Switch =>
         --  Automatically prints output.
         raise GNAT.Command_Line.Exit_From_Command_Line;
      when GNAT.Command_Line.Invalid_Parameter =>
         raise Invalid_Cmd_Line with "Missing argument for option '-"
           & GNAT.Command_Line.Full_Switch & "'";
   end;

   GNAT.Command_Line.Free (Config => Cmd_Conf);

   if not Ada.Directories.Exists (Name => To_String (Path_Stream)) then
      raise Inspect_Error with "File '" & To_String (Path_Stream)
        & "' not found";
   end if;

   declare
      Path_Str : constant String := To_String (Path_Stream);
   begin
      if Print_Sig then
         Print_Signature (Path => Path_Str);
      else
         Inspect (Path => Path_Str, Dump => Dump_Data);
      end if;
   end;

exception
   when E : Inspect_Error =>
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when GNAT.Command_Line.Exit_From_Command_Line =>
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : Invalid_Cmd_Line =>
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (X => E));
      Ada.Text_IO.Put_Line ("Try """ & Ada.Command_Line.Command_Name &
                              " --help"" for more information.");
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : others =>
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end SBS_Inspect;
