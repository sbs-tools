--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with SBS.Types;
with SBS.Config;

package Test_Utils
is

   use SBS;
   use type Ada.Streams.Stream_Element_Offset;

   --  Create (empty) test file of given size.
   procedure Create_File
     (Path : String;
      Size : Types.File_Size);

   Ref_Data_Block_A : constant Types.Block_Data_Type
     := (Types.Block_Data_Range'Last - 2 => 16#eb#,
         Types.Block_Data_Range'Last - 1 => 16#cc#,
         Types.Block_Data_Range'Last     => 16#12#,
         others                          => 0);
   Ref_Data_Block_B : constant Types.Block_Data_Type := (others => 2);
   Ref_Data_Block_C : constant Types.Block_Data_Type := (others => 3);

   Ref_File1_Size         : constant := 2 * Config.Block_Data_Size + 3;
   Ref_File1_Init_Padding : constant := Config.Block_Data_Size - 3;

   --  Create block reference file 1 containing the last three bytes of Block
   --  A and the complete block B|C data (padded case).
   procedure Create_Block_Ref_File1 (Path : String);

   Ref_File2_Size         : constant := 2 * Config.Block_Data_Size;
   Ref_File2_Init_Padding : constant := 0;

   --  Create block reference file 2 containing data of blocks B|C
   --  (unpadded case).
   procedure Create_Block_Ref_File2 (Path : String);

   Ref_Block_A_Hash : constant Types.Hashsum_Type
     := (16#C0#, 16#B5#, 16#6B#, 16#4D#, 16#B2#, 16#0E#, 16#58#, 16#17#,
         16#FE#, 16#64#, 16#00#, 16#03#, 16#BE#, 16#DF#, 16#40#, 16#D7#,
         16#C6#, 16#E7#, 16#B8#, 16#AE#, 16#71#, 16#53#, 16#81#, 16#FB#,
         16#EC#, 16#22#, 16#E0#, 16#C1#, 16#0D#, 16#BA#, 16#3E#, 16#D1#,
         16#5E#, 16#B9#, 16#3D#, 16#74#, 16#66#, 16#15#, 16#04#, 16#07#,
         16#91#, 16#08#, 16#07#, 16#E1#, 16#05#, 16#22#, 16#FB#, 16#37#,
         16#77#, 16#3C#, 16#DB#, 16#DE#, 16#3A#, 16#96#, 16#04#, 16#F4#,
         16#91#, 16#16#, 16#98#, 16#DF#, 16#67#, 16#F1#, 16#01#, 16#4A#);
   Ref_Block_B_Hash : constant Types.Hashsum_Type
     := (16#2B#, 16#1F#, 16#C0#, 16#12#, 16#EB#, 16#6E#, 16#B4#, 16#52#,
         16#F0#, 16#F3#, 16#B8#, 16#CC#, 16#7D#, 16#E7#, 16#C8#, 16#EF#,
         16#61#, 16#2B#, 16#03#, 16#6B#, 16#07#, 16#E6#, 16#3A#, 16#FB#,
         16#18#, 16#B9#, 16#AD#, 16#30#, 16#2E#, 16#1D#, 16#E5#, 16#99#,
         16#1B#, 16#F9#, 16#B1#, 16#69#, 16#39#, 16#CA#, 16#52#, 16#6E#,
         16#17#, 16#B2#, 16#52#, 16#43#, 16#C9#, 16#E9#, 16#C4#, 16#59#,
         16#C2#, 16#22#, 16#96#, 16#63#, 16#E4#, 16#70#, 16#EE#, 16#6E#,
         16#80#, 16#60#, 16#A0#, 16#51#, 16#D3#, 16#E5#, 16#B3#, 16#38#);
   Ref_Block_C_Hash : constant Types.Hashsum_Type
     := (16#B6#, 16#49#, 16#D0#, 16#64#, 16#B4#, 16#FD#, 16#3F#, 16#F6#,
         16#C2#, 16#27#, 16#49#, 16#CF#, 16#99#, 16#54#, 16#E1#, 16#7A#,
         16#3C#, 16#92#, 16#DE#, 16#5F#, 16#3C#, 16#88#, 16#F1#, 16#63#,
         16#72#, 16#20#, 16#DF#, 16#6F#, 16#E0#, 16#0D#, 16#EC#, 16#AE#,
         16#33#, 16#A6#, 16#EE#, 16#94#, 16#0F#, 16#66#, 16#1D#, 16#74#,
         16#F8#, 16#6B#, 16#82#, 16#86#, 16#96#, 16#8E#, 16#D2#, 16#51#,
         16#E1#, 16#E3#, 16#71#, 16#E7#, 16#98#, 16#D2#, 16#11#, 16#AF#,
         16#3B#, 16#8D#, 16#EE#, 16#D4#, 16#42#, 16#A2#, 16#5A#, 16#74#);

   Ref_Block_A : constant Types.Block_Type
     := (Hash_Of_Next_Block => Ref_Block_B_Hash,
         Data               => Ref_Data_Block_A);
   Ref_Block_B : constant Types.Block_Type
     := (Hash_Of_Next_Block => Ref_Block_C_Hash,
         Data               => Ref_Data_Block_B);
   Ref_Block_C : constant Types.Block_Type
     := (Hash_Of_Next_Block => (others => 0),
         Data               => Ref_Data_Block_C);

   --  Compare two files byte-wise. Returns True if both files are equal.
   --  The two files are closed but not removed after comparison. Raises
   --  Open_File_Error exception if one of the given files cannot be opened.
   function Equal_Files
     (Filename1 : String;
      Filename2 : String)
      return Boolean;

   Open_File_Error : exception;

end Test_Utils;
