#!/bin/bash

set -eu

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DATADIR=$(realpath $SCRIPTDIR/../data)
BINDIR=$(realpath $SCRIPTDIR/../bin)

SBS_CREATE=$BINDIR/sbs_create
SBS_INSPECT=$BINDIR/sbs_inspect

while true; do
	TMP=$(mktemp -d /tmp/sbsinspect.XXXXXX)
	INPUT=$TMP/dat
	SBS_IMG=$TMP/dat.sbs
	XTRACTED=$TMP/dat.x

	echo Results in $TMP
	dd if=/dev/urandom of=$INPUT bs=1KB count=$((1 + $RANDOM % 100000))
	$SBS_CREATE -k $DATADIR/gpg-homedir/ -i $INPUT -o $SBS_IMG
	$SBS_INSPECT -x -i $SBS_IMG > $XTRACTED
	diff -q $XTRACTED $INPUT
	rm -r $TMP
done
