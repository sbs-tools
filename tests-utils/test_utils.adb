--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Direct_IO;
with Ada.Streams.Stream_IO;

package body Test_Utils
is

   use Ada.Streams;

   --  Write common parts of block reference files and close file.
   procedure Create_Block_Common_And_Close (Fd : in out Stream_IO.File_Type);

   -------------------------------------------------------------------------

   procedure Create_File
     (Path : String;
      Size : Types.File_Size)
   is
      File : Stream_IO.File_Type;
      Data : constant Stream_Element_Array (1 .. Stream_Element_Offset (Size))
        := (others => 16#ff#);
   begin
      Stream_IO.Create
        (File => File,
         Mode => Stream_IO.Out_File,
         Name => Path);
      Stream_IO.Write
        (File => File,
         Item => Data);
      Stream_IO.Close (File => File);
   end Create_File;

   -------------------------------------------------------------------------

   procedure Create_Block_Common_And_Close (Fd : in out Stream_IO.File_Type)
   is
   begin
      Stream_IO.Write
        (File => Fd,
         Item => Ref_Data_Block_B);
      Stream_IO.Write
        (File => Fd,
         Item => Ref_Data_Block_C);
      Stream_IO.Close (File => Fd);
   end Create_Block_Common_And_Close;

   -------------------------------------------------------------------------

   procedure Create_Block_Ref_File1 (Path : String)
   is
      File : Stream_IO.File_Type;
      B1   : constant Stream_Element_Array
        := (1 => Ref_Data_Block_A (Types.Block_Data_Range'Last - 2),
            2 => Ref_Data_Block_A (Types.Block_Data_Range'Last - 1),
            3 => Ref_Data_Block_A (Types.Block_Data_Range'Last));
   begin
      Stream_IO.Create
        (File => File,
         Mode => Stream_IO.Out_File,
         Name => Path);
      Stream_IO.Write
        (File => File,
         Item => B1);
      Create_Block_Common_And_Close (Fd => File);
   end Create_Block_Ref_File1;

   -------------------------------------------------------------------------

   procedure Create_Block_Ref_File2 (Path : String)
   is
      File : Stream_IO.File_Type;
   begin
      Stream_IO.Create
        (File => File,
         Mode => Stream_IO.Out_File,
         Name => Path);
      Create_Block_Common_And_Close (Fd => File);
   end Create_Block_Ref_File2;

   -------------------------------------------------------------------------

   function Equal_Files
     (Filename1 : String;
      Filename2 : String)
      return Boolean
   is
      package DIO is new Ada.Direct_IO (Element_Type => Character);

      use type DIO.Count;

      --  Open file specified by filename.
      procedure Open_File
        (Filename :     String;
         File     : out DIO.File_Type);

      ----------------------------------------------------------------------

      procedure Open_File
        (Filename :     String;
         File     : out DIO.File_Type)
      is
      begin
         DIO.Open (File => File,
                   Mode => DIO.In_File,
                   Name => Filename,
                   Form => "shared=no");

      exception
         when others =>
            raise Open_File_Error with
              "Unable to open file '" & Filename & "'";
      end Open_File;

      File1, File2 : DIO.File_Type;
      Char1, Char2 : Character;
      Result       : Boolean := True;
   begin
      Open_File (Filename => Filename1,
                 File     => File1);
      Open_File (Filename => Filename2,
                 File     => File2);

      if DIO.Size (File1) /= DIO.Size (File2) then
         DIO.Close (File => File1);
         DIO.Close (File => File2);
         return False;
      end if;

      while not DIO.End_Of_File (File => File1) loop

         --  Read one byte from both files.

         DIO.Read (File => File1,
                   Item => Char1);
         DIO.Read (File => File2,
                   Item => Char2);

         if Char1 /= Char2 then
            Result := False;
         end if;
      end loop;

      DIO.Close (File => File1);
      DIO.Close (File => File2);

      return Result;
   end Equal_Files;

end Test_Utils;
