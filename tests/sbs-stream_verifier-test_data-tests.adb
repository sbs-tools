--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Stream_Verifier.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Unchecked_Conversion;
with Ada.Numerics.Discrete_Random;

with GNAT.OS_Lib;

with Interfaces.C;

with Test_Utils;
--  begin read only
--  end read only
package body SBS.Stream_Verifier.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_22cb26 (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/22cb26bf0979846f/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Streams.Stream_Element_Array;

      V  : Verifier_Type;
      RH : Types.Hashsum_Type := (others => 22);
   begin
      Init (Verifier    => V,
            Root_Hash   => RH,
            Block_Count => 3);

      Assert (Condition => V.Next_Hash = RH,
              Message   => "Root hash mismatch");
      Assert (Condition => V.Block_Count = 3,
              Message   => "Block count mismatch");
      Assert (Condition => V.Blocks_Checked = 0,
              Message   => "Checked count mismatch");
--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Update (Gnattest_T : in out Test);
   procedure Test_Update_794c83 (Gnattest_T : in out Test) renames Test_Update;
--  id:2.2/794c8327b19e2aad/Update/1/0/
   procedure Test_Update (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Streams.Stream_Element;

      subtype Block_Stream is Ada.Streams.Stream_Element_Array
        (1 .. Config.Block_Size);

      function Block_To_Stream is
        new Ada.Unchecked_Conversion
          (Source => Types.Block_Type,
           Target => Block_Stream);

      V          : Verifier_Type;
      Null_Hash  : constant Types.Hashsum_Type := (others => 0);
      All_Blocks : Ada.Streams.Stream_Element_Array
        (1 .. 3 * Config.Block_Size);
      Success    : Boolean;
   begin

      --  Without init called.

      Update (Verifier => V,
              Data     => Null_Hash,
              Success  => Success);
      Assert (Condition => not Success,
              Message   => "Successful without Init");

      All_Blocks (1 .. Config.Block_Size)
        := Block_To_Stream (Test_Utils.Ref_Block_A);
      All_Blocks (Config.Block_Size + 1 .. 2 * Config.Block_Size)
        := Block_To_Stream (Test_Utils.Ref_Block_B);
      All_Blocks (2 * Config.Block_Size + 1 .. 3 * Config.Block_Size)
        := Block_To_Stream (Test_Utils.Ref_Block_C);

      --  Block C only

      Init (Verifier    => V,
            Root_Hash   => Test_Utils.Ref_Block_C_Hash,
            Block_Count => 1);

      Update (Verifier => V,
              Data     => Null_Hash,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with hash (1)");
      Update (Verifier => V,
              Data     => Test_Utils.Ref_Data_Block_C,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with data (1)");
      Finalize (Verifier => V,
                Success  => Success);
      Assert (Condition => Success,
              Message   => "Finalize failed (1)");

      --  All Blocks, on a per-block basis

      Init (Verifier    => V,
            Root_Hash   => Test_Utils.Ref_Block_A_Hash,
            Block_Count => 3);

      --  Block A

      Update (Verifier => V,
              Data     => Test_Utils.Ref_Block_B_Hash,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with hash (2)");
      Update (Verifier => V,
              Data     => Test_Utils.Ref_Data_Block_A,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with data (2)");

      --  Block B

      Update (Verifier => V,
              Data     => Test_Utils.Ref_Block_C_Hash,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with hash (3)");
      Update (Verifier => V,
              Data     => Test_Utils.Ref_Data_Block_B,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with data (3)");

      --  Block C

      Update (Verifier => V,
              Data     => Null_Hash,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with hash (4)");
      Update (Verifier => V,
              Data     => Test_Utils.Ref_Data_Block_C,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with data (4)");

      Finalize (Verifier => V,
                Success  => Success);
      Assert (Condition => Success,
              Message   => "Finalize failed (2)");

      --  All Blocks, all at once

      Init (Verifier    => V,
            Root_Hash   => Test_Utils.Ref_Block_A_Hash,
            Block_Count => 3);

      Update (Verifier => V,
              Data     => All_Blocks,
              Success  => Success);
      Assert (Condition => Success,
              Message   => "Unable to update with all blocks");

      Finalize (Verifier => V,
                Success  => Success);
      Assert (Condition => Success,
              Message   => "Finalize failed (3)");

      --  All Blocks, with modified byte

      Init (Verifier    => V,
            Root_Hash   => Test_Utils.Ref_Block_A_Hash,
            Block_Count => 3);

      All_Blocks (2 * Config.Block_Size + 10)
        := All_Blocks (2 * Config.Block_Size + 10) + 1;

      Update (Verifier => V,
              Data     => All_Blocks,
              Success  => Success);
      Assert (Condition => not Success,
              Message   => "Modification not detected");
      Finalize (Verifier => V,
                Success  => Success);
      Assert (Condition => not Success,
              Message   => "Finalize successful");

      All_Blocks (2 * Config.Block_Size + 10)
        := All_Blocks (2 * Config.Block_Size + 10) - 1;

      --  Random chunks

      declare
         use type GNAT.OS_Lib.Process_Id;

         subtype Random_Range is Buffer_Range range 1 .. Buffer_Range'Last / 1024;

         subtype All_Blocks_Range is Ada.Streams.Stream_Element_Offset range
           0 .. 3 * Config.Block_Size;

         package Rand is new Ada.Numerics.Discrete_Random (Random_Range);

         Generator : Rand.Generator;
         Pid       : constant GNAT.OS_Lib.Process_Id
           := GNAT.OS_Lib.Current_Process_Id;

         Pos_S, Pos_E, Tmp : All_Blocks_Range := 0;
      begin
         Assert (Condition => Pid /= GNAT.OS_Lib.Invalid_Pid,
                 Message   => "Process ID invalid");

         Init (Verifier    => V,
               Root_Hash   => Test_Utils.Ref_Block_A_Hash,
               Block_Count => 3);
         Rand.Reset (Gen       => Generator,
                     Initiator => GNAT.OS_Lib.Pid_To_Integer (Pid => Pid));

         while Pos_E /= All_Blocks_Range'Last loop
            Pos_S := Pos_E + 1;
            Tmp   := Rand.Random (Gen => Generator);

            if Positive (Pos_S + Tmp) > Positive (All_Blocks_Range'Last) then
               Pos_E := All_Blocks_Range'Last;
            else
               Pos_E := Pos_S + Tmp;
            end if;

            Update (Verifier => V,
                    Data     => All_Blocks (Pos_S .. Pos_E),
                    Success  => Success);
            Assert (Condition => Success,
                    Message   => "Chunk not successful");
         end loop;

         Finalize (Verifier => V,
                   Success  => Success);
         Assert (Condition => Success,
                 Message   => "Finalize failed (4)");
      end;
--  begin read only
   end Test_Update;
--  end read only


--  begin read only
   procedure Test_Finalize (Gnattest_T : in out Test);
   procedure Test_Finalize_7140f8 (Gnattest_T : in out Test) renames Test_Finalize;
--  id:2.2/7140f8c2180bfbce/Finalize/1/0/
   procedure Test_Finalize (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      V    : Verifier_Type;
      Succ : Boolean;
   begin
      V.Block_Count    := 2;
      V.Blocks_Checked := 2;

      Finalize (Verifier => V,
                Success  => Succ);
      Assert (Condition => Succ,
              Message   => "Not successful");

      V.Block_Count    := 2;
      V.Blocks_Checked := 1;

      Finalize (Verifier => V,
                Success  => Succ);
      Assert (Condition => not Succ,
              Message   => "Successful");
--  begin read only
   end Test_Finalize;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Stream_Verifier.Test_Data.Tests;
