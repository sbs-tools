--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Reverse_Reader.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Directories;
with Ada.Exceptions;
with Ada.Streams;

with SBS.Config;

with Test_Utils;
--  begin read only
--  end read only
package body SBS.Reverse_Reader.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Get_Bytes_To_Read (Gnattest_T : in out Test);
   procedure Test_Get_Bytes_To_Read_97f40f (Gnattest_T : in out Test) renames Test_Get_Bytes_To_Read;
--  id:2.2/97f40f2130e92cb1/Get_Bytes_To_Read/1/0/
   procedure Test_Get_Bytes_To_Read (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      Initialized := True;

      Bytes_Remaining := 0;
      Assert (Condition => Get_Bytes_To_Read = 0,
              Message   => "Unexpected value (1)");
      Bytes_Remaining := 12;
      Assert (Condition => Get_Bytes_To_Read = 12,
              Message   => "Unexpected value (2)");

      Initialized := False;

   exception
      when others =>
         Initialized := False;
--  begin read only
   end Test_Get_Bytes_To_Read;
--  end read only


--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_5c0dc3 (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/5c0dc3fb3517b5ff/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type SBS.Types.File_Size;

      Fn : constant String := "obj/reader_init";
   begin
      Test_Utils.Create_File (Path => Fn,
                              Size => 12);

      Init (Path          => Fn,
            Bytes_To_Read => 12);
      Assert (Condition => Initialized,
              Message   => "Not initialized");
      Assert (Condition => Bytes_Remaining = 12,
              Message   => "Bytes remaining mismatch");
      Assert (Condition => Initial_Padding = Config.Block_Data_Size - 12,
              Message   => "Padding mismatch:" & Initial_Padding'Img);

      Close;

      Assert (Condition => not Initialized,
              Message   => "Still initialized");

      Ada.Directories.Delete_File (Name => Fn);
--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Get_Initial_Padding (Gnattest_T : in out Test);
   procedure Test_Get_Initial_Padding_9541f2 (Gnattest_T : in out Test) renames Test_Get_Initial_Padding;
--  id:2.2/9541f26ce7b844d3/Get_Initial_Padding/1/0/
   procedure Test_Get_Initial_Padding (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      Initialized := True;

      Initial_Padding := 0;
      Assert (Condition => Get_Initial_Padding = 0,
              Message   => "Unexpected value (1)");
      Initial_Padding := 1222;
      Assert (Condition => Get_Initial_Padding = 1222,
              Message   => "Unexpected value (2)");

      Initialized := False;

   exception
      when others =>
         Initialized := False;
         raise;
--  begin read only
   end Test_Get_Initial_Padding;
--  end read only


--  begin read only
   procedure Test_Get_Next_Block (Gnattest_T : in out Test);
   procedure Test_Get_Next_Block_4153c5 (Gnattest_T : in out Test) renames Test_Get_Next_Block;
--  id:2.2/4153c573272ed74f/Get_Next_Block/1/0/
   procedure Test_Get_Next_Block (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type SBS.Types.File_Size;
      use type SBS.Types.Block_Data_Type;

      Fn   : constant String := "obj/reader_get_block";
      B    : aliased Types.Block_Data_Type;
      Size : Types.File_Size := Test_Utils.Ref_File1_Size;
   begin

      --  Padded case.

      Test_Utils.Create_Block_Ref_File1 (Path => Fn);

      Init (Path          => Fn,
            Bytes_To_Read => Size);
      Assert (Condition => Bytes_Remaining = Size,
              Message   => "File1: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);
      Assert (Condition => Initial_Padding = Test_Utils.Ref_File1_Init_Padding,
              Message   => "File1: Initial padding mismatch:"
              & Initial_Padding'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => B = Test_Utils.Ref_Data_Block_C,
              Message   => "File1: Block 3 mismatch");
      Assert (Condition => Bytes_Remaining = Size - Config.Block_Data_Size,
              Message   => "File1: Bytes remaining mismatch"
              & Bytes_Remaining'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => B = Test_Utils.Ref_Data_Block_B,
              Message   => "File1: Block 2 mismatch");
      Assert (Condition => Bytes_Remaining = Size - 2 * Config.Block_Data_Size,
              Message   => "File1: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => B = Test_Utils.Ref_Data_Block_A,
              Message   => "File1: Block 1 mismatch");
      Assert (Condition => Bytes_Remaining = 0,
              Message   => "File1: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);

      Close;
      Ada.Directories.Delete_File (Name => Fn);

      --  Unpadded case.

      Size := Test_Utils.Ref_File2_Size;
      Test_Utils.Create_Block_Ref_File2 (Path => Fn);

      Init (Path          => Fn,
            Bytes_To_Read => Size);
      Assert (Condition => Bytes_Remaining = Size,
              Message   => "File2: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);
      Assert (Condition => Initial_Padding = 0,
              Message   => "File2: Initial padding mismatch:"
              & Initial_Padding'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => B = Test_Utils.Ref_Data_Block_C,
              Message   => "File2: Block 3 mismatch");
      Assert (Condition => Bytes_Remaining = Size - Config.Block_Data_Size,
              Message   => "File2: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => B = Test_Utils.Ref_Data_Block_B,
              Message   => "File2: Block 2 mismatch");
      Assert (Condition => Bytes_Remaining = Size - 2 * Config.Block_Data_Size,
              Message   => "File2: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);

      Close;
      Ada.Directories.Delete_File (Name => Fn);

      --  Read less than file size.

      Size := 2;
      Test_Utils.Create_Block_Ref_File2 (Path => Fn);

      Init (Path          => Fn,
            Bytes_To_Read => Size);
      Assert (Condition => Bytes_Remaining = Size,
              Message   => "Incomplete: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);
      Assert (Condition => Initial_Padding = Config.Block_Data_Size - 2,
              Message   => "Incomplete: Initial padding mismatch:"
              & Initial_Padding'Img);

      Get_Next_Block (Item => B'Access);
      Assert (Condition => Bytes_Remaining = 0,
              Message   => "Incomplete: Bytes remaining mismatch:"
              & Bytes_Remaining'Img);

      declare
         use type Ada.Streams.Stream_Element_Offset;

         Ref_Data_Block : constant Types.Block_Data_Type
           := (Types.Block_Data_Range'Last - 1 => 2,
               Types.Block_Data_Range'Last     => 2,
               others                          => 0);
      begin
         Assert (Condition => B = Ref_Data_Block,
                 Message   => "Incomplete: Block 1 mismatch");
      end;

      Close;
      Ada.Directories.Delete_File (Name => Fn);

      --  Try to read more than file size (non-padded).

      Size := Config.Block_Data_Size + 1;
      Test_Utils.Create_File (Path => Fn,
                              Size => Config.Block_Data_Size);

      Init (Path          => Fn,
            Bytes_To_Read => Size);

      begin
         for I in 1 .. 10 loop
            Get_Next_Block (Item => B'Access);
         end loop;
         Assert (Condition => False,
                 Message   => "Exception expected (1)");

      exception
         when E : Read_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to read complete block of 65472 bytes, only "
                      & "65471 read",
                Message   => "Exception mismatch (1)");
      end;

      Close;
      Ada.Directories.Delete_File (Name => Fn);

      --  Try to read more than file size (padded).

      Size := 65;
      Test_Utils.Create_File
        (Path => Fn,
         Size => 12);

      Init (Path          => Fn,
            Bytes_To_Read => Size);

      begin
         for I in 1 .. 10 loop
            Get_Next_Block (Item => B'Access);
         end loop;
         Assert (Condition => False,
                 Message   => "Exception expected (2)");

      exception
         when E : Read_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to read partial block of 65 byte(s), only 12 "
                    & "read",
                    Message   => "Exception mismatch (2)");
      end;

      Close;
      Ada.Directories.Delete_File (Name => Fn);
--  begin read only
   end Test_Get_Next_Block;
--  end read only


--  begin read only
   procedure Test_Close (Gnattest_T : in out Test);
   procedure Test_Close_e0b8a0 (Gnattest_T : in out Test) renames Test_Close;
--  id:2.2/e0b8a0a7e3fcad97/Close/1/0/
   procedure Test_Close (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      Assert (Condition => True,
              Message   => "Tested in Init test");
--  begin read only
   end Test_Close;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Reverse_Reader.Test_Data.Tests;
