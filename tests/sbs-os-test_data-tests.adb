--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.OS.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Exceptions;
with Ada.Strings.Unbounded;
--  begin read only
--  end read only
package body SBS.OS.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_1_Execute (Gnattest_T : in out Test);
   procedure Test_Execute_be370a (Gnattest_T : in out Test) renames Test_1_Execute;
--  id:2.2/be370a9becf523af/Execute/1/0/
   procedure Test_1_Execute (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin

      --  Positive test, no exception expected.

      Execute (Command => "/bin/true");

      begin
         Execute (Command => "/bin/false");
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when E : Command_Failed =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Execution of command '/bin/false' failed",
                    Message   => "Exception mismatch");
      end;
--  begin read only
   end Test_1_Execute;
--  end read only


--  begin read only
   procedure Test_2_Execute (Gnattest_T : in out Test);
   procedure Test_Execute_5a4848 (Gnattest_T : in out Test) renames Test_2_Execute;
--  id:2.2/5a4848db3799d9d9/Execute/0/0/
   procedure Test_2_Execute (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Strings.Unbounded;

      Output : Unbounded_String;
      Str    : constant String := "This is a test output";
   begin
      Execute (Command => "/bin/echo " & Str,
               Output  => Output);
      Assert (Condition => Output = Str & ASCII.LF,
              Message   => "Output mismatch (stdout)");
      Execute (Command => "data/stderr.sh",
               Output  => Output);
      Assert (Condition => Output = "stderr output",
              Message   => "Output mismatch (stderr)");

      begin
         Execute (Command => "data/fail.sh",
                  Output  => Output);
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when Command_Failed =>
            Assert (Condition => Output = "Line 1",
                    Message   => "Output mismatch (fail)");
      end;
--  begin read only
   end Test_2_Execute;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.OS.Test_Data.Tests;
