--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Exceptions;
with Ada.Directories;

with SBS.Config;
with SBS.Types;

with Test_Utils;
--  begin read only
--  end read only
package body SBS.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Process (Gnattest_T : in out Test);
   procedure Test_Process_85d94c (Gnattest_T : in out Test) renames Test_Process;
--  id:2.2/85d94cd9316dd346/Process/1/0/
   procedure Test_Process (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type SBS.Types.File_Size;
   begin
      begin
         Process (Input_File  => "/non_existent_file",
                  Key_Path    => "data/gpg-homedir",
                  Output_File => "output");
         Assert (Condition => False,
                 Message   => "Exception expected (1)");

      exception
         when E : Processing_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Input file '/non_existent_file' does not exist",
                    Message   => "Exception mismatch (1)");
      end;

      Test_Utils.Create_File (Path => "obj/process_input1",
                              Size => 2 * Config.Block_Size);

      begin
         Process (Input_File  => "obj/process_input1",
                  Key_Path    => "/no_such_key",
                  Output_File => "output");
         Assert (Condition => False,
                 Message   => "Exception expected (2)");

      exception
         when E : Processing_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Key path '/no_such_key' does not exist",
                    Message   => "Exception mismatch (2)");
      end;

      Process (Input_File  => "obj/process_input1",
               Key_Path    => "data/gpg-homedir",
               Output_File => "obj/process_output1");

      Ada.Directories.Delete_File (Name => "obj/process_input1");
      Ada.Directories.Delete_File (Name => "obj/process_output1");
--  begin read only
   end Test_Process;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Test_Data.Tests;
