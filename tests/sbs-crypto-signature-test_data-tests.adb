--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Crypto.Signature.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Text_IO;
with Ada.Directories;
with Ada.Direct_IO;
with Ada.Streams.Stream_IO; use Ada.Streams;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Interfaces;

with SBS.OS;
with SBS.Utils;
with SBS.Crypto.Signature.GPG;
--  begin read only
--  end read only
package body SBS.Crypto.Signature.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Sign (Gnattest_T : in out Test);
   procedure Test_Sign_3bf2b6 (Gnattest_T : in out Test) renames Test_Sign;
--  id:2.2/3bf2b629640db418/Sign/1/0/
   procedure Test_Sign (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      ----------------------------------------------------------------------

      procedure No_Key
      is
         H : constant Types.Header_Type := Types.Default_Header;
      begin
         begin
            declare
               Dummy : constant Stream_Element_Array
                 := Sign (Key_Path => "/dev/null",
                          Header   => H);
            begin
               Assert (Condition => False,
                       Message   => "Exception expected");
            end;

         exception
            when Sign_Error => null;
         end;
      end No_Key;

      ----------------------------------------------------------------------

      procedure Positive
      is
         GPG_Home : constant String := "data/gpg-homedir";
         Hdr_Name : constant String := "hdr";
         Hdr_Path : constant String := "obj/" & Hdr_Name;
         Sig_Name : constant String := "obj/" & Hdr_Name & ".sig";

         Hdr : Types.Header_Type := Types.Default_Header;
      begin

         --  A gpg signature contains a nonce so it is not possible to compare
         --  the generated signature with a reference.sig file. Use gpg command
         --  to verify generated signature instead.

         Hdr.Root_Hash   := (others => 16#bb#);
         Hdr.Block_Count := 12;

         Utils.Write_Hdr
           (Hdr  => Hdr,
            Path => Hdr_Path);

         declare
            Fd : Stream_IO.File_Type;
         begin
            Stream_IO.Create (File => Fd,
                              Mode => Stream_IO.Out_File,
                              Name => Sig_Name);
            Stream_IO.Write (File => Fd,
                             Item => Sign
                               (Key_Path => GPG_Home,
                                Header   => Hdr));
            Stream_IO.Close (File => Fd);

            --  Must not raise an exception.

            declare
               Cmd_Out : Unbounded_String;
            begin
               OS.Execute (Command => "gpg --batch --verify --homedir "
                           & GPG_Home & " " & Sig_Name & " " & Hdr_Path,
                           Output  => Cmd_Out);
               Ada.Text_IO.Put_Line (To_String (Cmd_Out));

            exception
               when OS.Command_Failed =>
                  Assert (Condition => False,
                          Message   => To_String (Cmd_Out));
            end;

            Ada.Directories.Delete_File (Name => Sig_Name);
            Ada.Directories.Delete_File (Name => Hdr_Path);
         end;
      end Positive;
   begin
      Positive;
      No_Key;
--  begin read only
   end Test_Sign;
--  end read only


--  begin read only
   procedure Test_Get_Signature_Size (Gnattest_T : in out Test);
   procedure Test_Get_Signature_Size_486b84 (Gnattest_T : in out Test) renames Test_Get_Signature_Size;
--  id:2.2/486b8438e88f9582/Get_Signature_Size/1/0/
   procedure Test_Get_Signature_Size (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      Assert (Condition => Get_Signature_Size
              (Key_Path => "data/gpg-homedir")
              = GPG.GPG_Max_RSA4096_Sig_Length,
              Message   => "Signature size mismatch");
--  begin read only
   end Test_Get_Signature_Size;
--  end read only


--  begin read only
   procedure Test_Set_Scheme (Gnattest_T : in out Test);
   procedure Test_Set_Scheme_a25471 (Gnattest_T : in out Test) renames Test_Set_Scheme;
--  id:2.2/a25471a76c1f407a/Set_Scheme/1/0/
   procedure Test_Set_Scheme (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Interfaces.Unsigned_16;

      Hdr : Types.Header_Type;
   begin
      Hdr.Signature_Scheme_ID := 99;

      Set_Scheme (Header => Hdr);
      Assert (Condition => Hdr.Signature_Scheme_ID = 1,
              Message   => "Signature scheme mismatch");
--  begin read only
   end Test_Set_Scheme;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Crypto.Signature.Test_Data.Tests;
