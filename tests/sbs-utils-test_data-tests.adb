--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Utils.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Test_Utils;
--  begin read only
--  end read only
package body SBS.Utils.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Get_Block_Count (Gnattest_T : in out Test);
   procedure Test_Get_Block_Count_07eeb7 (Gnattest_T : in out Test) renames Test_Get_Block_Count;
--  id:2.2/07eeb7b7faff24b3/Get_Block_Count/1/0/
   procedure Test_Get_Block_Count (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      Assert (Condition => Get_Block_Count
              (Size => SBS.Config.Block_Data_Size) = 1,
              Message   => "Block count mismatch (1)");
      Assert (Condition => Get_Block_Count
              (Size => 4 * SBS.Config.Block_Data_Size) = 4,
              Message   => "Block count mismatch (2)");
      Assert (Condition => Get_Block_Count
              (Size => 10 * SBS.Config.Block_Data_Size) + 1 = 11,
              Message   => "Block count mismatch (3)");

      declare
         Dummy : Positive;
      begin
         Dummy := Get_Block_Count (Size => Ada.Directories.File_Size'Last);
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when System.Assertions.Assert_Failure => null;
      end;
--  begin read only
   end Test_Get_Block_Count;
--  end read only


--  begin read only
   procedure Test_To_Hex_Str (Gnattest_T : in out Test);
   procedure Test_To_Hex_Str_6f3222 (Gnattest_T : in out Test) renames Test_To_Hex_Str;
--  id:2.2/6f32222f8aeceabb/To_Hex_Str/1/0/
   procedure Test_To_Hex_Str (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Ref_Hash_Block_B : constant String
        := "2b1fc012eb6eb452f0f3b8cc7de7c8ef612b036b07e63afb18b9ad302e1de5991b"
        & "f9b16939ca526e17b25243c9e9c459c2229663e470ee6e8060a051d3e5b338";
   begin
      Assert (Condition => To_Hex_Str (A => (16#eb#, 16#22#, 16#cc#))
              = "eb22cc",
              Message   => "Hex string mismatch (1)");
      Assert (Condition => To_Hex_Str (A => Test_Utils.Ref_Block_B_Hash)
              = Ref_Hash_Block_B,
              Message   => "Hex string mismatch (2)");
--  begin read only
   end Test_To_Hex_Str;
--  end read only


--  begin read only
   procedure Test_Hex_To_Bytes (Gnattest_T : in out Test);
   procedure Test_Hex_To_Bytes_a9297c (Gnattest_T : in out Test) renames Test_Hex_To_Bytes;
--  id:2.2/a9297c0130f2245d/Hex_To_Bytes/1/0/
   procedure Test_Hex_To_Bytes (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Ada.Streams.Stream_Element_Array;

      Str : constant String (12 .. 17) := "abcd12";
   begin
      Assert (Condition => Hex_To_Bytes (Str => "11aa12")
              = (16#11#, 16#aa#, 16#12#),
              Message   => "Bytes mismatch (1)");
      Assert (Condition => Hex_To_Bytes (Str => "00000000")
              = (16#00#, 16#00#, 16#00#, 16#00#),
              Message   => "Bytes mismatch (2)");
      Assert (Condition => Hex_To_Bytes (Str => Str)
              = (16#ab#, 16#cd#, 16#12#),
              Message   => "Bytes mismatch (3)");

      begin
         declare
            Dummy : constant Ada.Streams.Stream_Element_Array
              := Hex_To_Bytes (Str => "no hex");
         begin
            Assert (Condition => False,
                    Message   => "Exception expected");
         end;

      exception
         when Conversion_Error => null;
      end;
--  begin read only
   end Test_Hex_To_Bytes;
--  end read only


--  begin read only
   procedure Test_Write_Hdr (Gnattest_T : in out Test);
   procedure Test_Write_Hdr_8d1404 (Gnattest_T : in out Test) renames Test_Write_Hdr;
--  id:2.2/8d14044c7a41b293/Write_Hdr/1/0/
   procedure Test_Write_Hdr (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Fn  : constant String            := "obj/write_hdr";
      Hdr : constant Types.Header_Type := Types.Default_Header;
   begin
      Write_Hdr (Hdr  => Hdr,
                 Path => Fn);
      Assert (Condition => Test_Utils.Equal_Files
              (Filename1 => Fn,
               Filename2 => "data/write_hdr"),
              Message   => "Header mismatch");
      Ada.Directories.Delete_File (Name => Fn);
--  begin read only
   end Test_Write_Hdr;
--  end read only


--  begin read only
   procedure Test_Create_Tempdir (Gnattest_T : in out Test);
   procedure Test_Create_Tempdir_3449e0 (Gnattest_T : in out Test) renames Test_Create_Tempdir;
--  id:2.2/3449e0544c9a01c8/Create_Tempdir/1/0/
   procedure Test_Create_Tempdir (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Dir : constant String := Create_Tempdir;
   begin
      Assert (Condition => Ada.Directories.Exists (Name => Dir),
              Message   => "Tmpdir missing");
      Ada.Directories.Delete_Tree (Directory => Dir);
--  begin read only
   end Test_Create_Tempdir;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Utils.Test_Data.Tests;
