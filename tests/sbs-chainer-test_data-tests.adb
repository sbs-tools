--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Chainer.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with System;

with Ada.Exceptions;
with Ada.Directories;
with Ada.Streams.Stream_IO;

with Interfaces;

with SBS.Utils;
with SBS.Config;
with SBS.Types;
with SBS.Mmap;
with SBS.Crypto.Signature;

with Test_Utils;
--  begin read only
--  end read only
package body SBS.Chainer.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Run (Gnattest_T : in out Test);
   procedure Test_Run_e5a2dd (Gnattest_T : in out Test) renames Test_Run;
--  id:2.2/e5a2dd86b12d7902/Run/1/0/
   procedure Test_Run (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use Ada.Streams;
      use type Interfaces.Unsigned_16;
      use type Interfaces.Unsigned_32;
      use type SBS.Types.File_Size;

      Blocks   : constant Positive
        := Utils.Get_Block_Count (Size => Test_Utils.Ref_File1_Size);
      In_File  : constant String := "obj/chainer_input1";
      Key_Path : constant String := "data/gpg-homedir";
      Out_File : constant String := "obj/chainer_output1";
      Sig_Len  : constant Positive
        := Crypto.Signature.Get_Signature_Size (Key_Path => Key_Path);
      Out_Size : constant Types.File_Size := Types.File_Size
        (Blocks) * Config.Block_Size + Config.Header_Size + Types.File_Size
        (Sig_Len);

      Header_Buffer : Stream_Element_Array (1 .. Config.Header_Size);
      Last          : Stream_Element_Offset;

      Fd : Stream_IO.File_Type;
   begin
      Test_Utils.Create_File (Path => In_File,
                              Size => 0);

      begin
         Run (Input_File  => In_File,
              Key_Path    => Key_Path,
              Output_File => Out_File);
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when E : Chainer_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Input file 'obj/chainer_input1' is empty, no data to "
                    & "process",
                    Message   => "Exception mismatch");
      end;

      Test_Utils.Create_Block_Ref_File1 (Path => In_File);

      Run (Input_File  => In_File,
           Key_Path    => Key_Path,
           Output_File => Out_File);
      Ada.Directories.Delete_File (Name => In_File);

      Assert (Condition => Ada.Directories.Size (Name => Out_File) = Out_Size,
              Message   => "Unexpected file size");

      Stream_IO.Open (File => Fd,
                      Mode => Stream_IO.In_File,
                      Name => Out_File);

      Stream_IO.Read (File => Fd,
                      Item => Header_Buffer,
                      Last => Last);
      Assert (Condition => Last = Config.Header_Size,
              Message   => "Reading header failed");

      declare
         use type Config.Hash_Algorithm_Type;
         use type Config.Signature_Scheme_Type;

         Header : Types.Header_Type
           with
             Address => Header_Buffer'Address;
      begin
         Assert
           (Condition => Header.Version_Magic = Config.Version_Magic,
            Message   => "Version magic mismatch");
         Assert
           (Condition => Header.Block_Count = Interfaces.Unsigned_32 (Blocks),
            Message   => "Block count mismatch");
         Assert
           (Condition => Header.Block_Size = Config.Block_Size,
            Message   => "Block size mismatch");
         Assert
           (Condition => Header.Header_Size = Config.Header_Size,
            Message   => "Header size mismatch");
         Assert
           (Condition => Header.Hashsum_Length = Config.Hashsum_Length,
            Message   => "Hashsum length mismatch");
         Assert
           (Condition => Header.Padding_Length =
              Test_Utils.Ref_File1_Init_Padding,
            Message   => "Padding mismatch");
         Assert
           (Condition => Header.Root_Hash = Test_Utils.Ref_Block_A_Hash,
            Message   => "Root hash mismatch");

         Assert (Condition => Config.Signature_Scheme_Type'Enum_Val
                 (Header.Signature_Scheme_ID) = Config.Signature_PGP,
                 Message   => "Signature scheme mismatch");

         Assert (Condition => Config.Hash_Algorithm_Type'Val
                 (Header.Hash_Algorithm_ID_1) = Config.Hash_SHA2_512,
                 Message   => "Hash algo 1 mismatch");
         Assert (Condition => Config.Hash_Algorithm_Type'Val
                 (Header.Hash_Algorithm_ID_2) = Config.Hash_None,
                 Message   => "Hash algo 2 mismatch");
         Assert (Condition => Config.Hash_Algorithm_Type'Val
                 (Header.Hash_Algorithm_ID_3) = Config.Hash_None,
                 Message   => "Hash algo 3 mismatch");
         Assert (Condition => Config.Hash_Algorithm_Type'Val
                 (Header.Hash_Algorithm_ID_4) = Config.Hash_None,
                 Message   => "Hash algo 4 mismatch");
      end;

      declare
         Signature : Stream_Element_Array
           (1 .. Stream_Element_Offset (Sig_Len));
      begin
         Stream_IO.Read (File => Fd,
                         Item => Signature,
                         Last => Last);
         Assert (Condition => Last = Stream_Element_Offset (Sig_Len),
                 Message   => "Reading signature failed");
      end;

      declare
         use type SBS.Types.Block_Type;

         Block_Buffer : Stream_Element_Array (1 .. Config.Block_Size);
      begin
         Stream_IO.Read (File => Fd,
                         Item => Block_Buffer,
                         Last => Last);
         Assert (Condition => Last = Config.Block_Size,
                 Message   => "Reading block1 failed");

         for I in Stream_Element_Offset range
           Config.Hashsum_Length + 1 .. Test_Utils.Ref_File1_Init_Padding
         loop
            Assert (Condition => Block_Buffer (I) = 0,
                    Message   => "Block 1 padding byte" & I'Img & " not zero:"
                    & Block_Buffer (I)'Img);
         end loop;

         declare
            Block1 : Types.Block_Type
              with
                Address => Block_Buffer'Address;
         begin
            Assert (Condition => Block1 = Test_Utils.Ref_Block_A,
                    Message   => "Block 1 mismatch");
         end;

         Stream_IO.Read (File => Fd,
                         Item => Block_Buffer,
                         Last => Last);
         Assert (Condition => Last = Config.Block_Size,
                 Message   => "Reading block2 failed");

         declare
            Block2 : Types.Block_Type
              with
                Address => Block_Buffer'Address;
         begin
            Assert (Condition => Block2 = Test_Utils.Ref_Block_B,
                    Message   => "Block 2 mismatch");
         end;

         Stream_IO.Read (File => Fd,
                         Item => Block_Buffer,
                         Last => Last);
         Assert (Condition => Last = Config.Block_Size,
                 Message   => "Reading block3 failed");

         declare
            Block3 : Types.Block_Type
              with
                Address => Block_Buffer'Address;
         begin
            Assert (Condition => Block3 = Test_Utils.Ref_Block_C,
                    Message   => "Block 3 mismatch");
         end;
      end;

      declare
         One : Stream_Element_Array (1 ..1);
      begin
         Stream_IO.Read (File => Fd,
                         Item => One,
                         Last => Last);
         Assert (Condition => Last = 0,
                 Message   => "Not all bytes consumed by chainer");
      end;


      Ada.Directories.Delete_File (Name => Out_File);

   exception
      when others =>
         if Stream_IO.Is_Open (File => Fd) then
            Stream_IO.Close (File => Fd);
         end if;
         raise;
--  begin read only
   end Test_Run;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Chainer.Test_Data.Tests;
