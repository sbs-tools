--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Mmap.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Exceptions;
with Ada.Directories;
with Ada.Sequential_IO;

with Interfaces;
--  begin read only
--  end read only
package body SBS.Mmap.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Mmap_File (Gnattest_T : in out Test);
   procedure Test_Mmap_File_b1cd08 (Gnattest_T : in out Test) renames Test_Mmap_File;
--  id:2.2/b1cd08567bee55a1/Mmap_File/1/0/
   procedure Test_Mmap_File (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Map_Address : System.Address;
      Filename    : constant String := "obj/mmap_file1";
      Ref_Value   : constant        := 16#dead_beef_dead_beef#;
   begin
      begin
         Mmap_File (Filename => "/no_such_file",
                    Size     => Interfaces.Unsigned_64'Object_Size,
                    Mode     => Mmap.In_File,
                    Address  => Map_Address);
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when E : Mmap_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error opening file /no_such_file",
                    Message   => "Exception mismatch");
      end;

      Mmap_File (Filename => Filename,
                 Size     => Interfaces.Unsigned_64'Object_Size,
                 Mode     => Mmap.Out_File,
                 Address  => Map_Address);

      declare
         Value : Interfaces.Unsigned_64
           with
             Address => Map_Address;
      begin
         Value := Ref_Value;
      end;

      Munmap (Address => Map_Address,
              Size    => Interfaces.Unsigned_64'Object_Size);

      declare
         use type Interfaces.Unsigned_64;

         package SIO is new Ada.Sequential_IO
           (Element_Type => Interfaces.Unsigned_64);

         FD    : SIO.File_Type;
         Value : Interfaces.Unsigned_64;
      begin
         SIO.Open (File => FD,
                   Mode => SIO.In_File,
                   Name => Filename);
         SIO.Read (File => FD,
                   Item => Value);
         SIO.Close (File => FD);

         Assert (Condition => Value = Ref_Value,
                 Message   => "Value mismatch");
      end;

      Ada.Directories.Delete_File (Name => Filename);
--  begin read only
   end Test_Mmap_File;
--  end read only


--  begin read only
   procedure Test_Munmap (Gnattest_T : in out Test);
   procedure Test_Munmap_f10628 (Gnattest_T : in out Test) renames Test_Munmap;
--  id:2.2/f106287266f92e4f/Munmap/1/0/
   procedure Test_Munmap (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      begin
         Munmap (Address => System'To_Address (Interfaces.Unsigned_64'Last),
                 Size    => 12);
         Assert (Condition => False,
                 Message   => "Exception expected");

      exception
         when E : Mmap_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Munmap failed - Invalid argument",
                    Message   => "Exception mismatch");
      end;
--  begin read only
   end Test_Munmap;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Mmap.Test_Data.Tests;
