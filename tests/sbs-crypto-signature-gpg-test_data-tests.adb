--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Crypto.Signature.GPG.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Exceptions;
--  begin read only
--  end read only
package body SBS.Crypto.Signature.GPG.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Get_Secret_Key_Info (Gnattest_T : in out Test);
   procedure Test_Get_Secret_Key_Info_f278d1 (Gnattest_T : in out Test) renames Test_Get_Secret_Key_Info;
--  id:2.2/f278d17835165eb1/Get_Secret_Key_Info/1/0/
   procedure Test_Get_Secret_Key_Info (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      Ref1 : constant String
        := "gpg: WARNING: unsafe permissions on homedir '/tmp/key_tests/gpg'"
        & ASCII.LF & "tmp/key_tests/gpg/pubring.kbx"
        & ASCII.LF & "------------------------------"
        & ASCII.LF & "sec   rsa4096 2020-01-18 [SC]"
        & ASCII.LF & "      D33D54C05323928EB6255993118164747DDC931C"
        & ASCII.LF & "uid           [ultimate] testkey1 <test@test.ch>";
      Ref2 : constant String
        := Ref1 & ASCII.LF
        & ASCII.LF & "sec   rsa3072 2020-01-18 [SC]"
        & ASCII.LF & "      BEF38C5847DF3F95AB4A256B6044BA8AF70CFFE"
        & ASCII.LF & "uid           [ultimate] testkey2 <testkey2@test.ch>"
        & ASCII.LF;
      Ref3 : constant String
        := "tmp/key_tests/gpg/pubring.kbx"
        & ASCII.LF & "------------------------------"
        & ASCII.LF & "sec   rsa1024 2020-01-18 [SC]"
        & ASCII.LF & "      D33D54C05323928EB6255993118164747DDC931C"
        & ASCII.LF & "uid           [ultimate] testkey1 <test@test.ch>";

      Key : Seckey_Type;
   begin
      begin
         Key := Get_Secret_Key_Info (Seckey_List => "<no seckey list>");
         Assert (Condition => False,
                 Message   => "Exception expected (1)");

      exception
         when E : Sign_Key_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "No secret key found in given keyring",
                    Message   => "Exception mismatch (1)");
      end;

      begin
         Key := Get_Secret_Key_Info (Seckey_List => "sec   ");
         Assert (Condition => False,
                 Message   => "Exception expected (2)");

      exception
         when E : Sign_Key_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error extracting secret key properties",
                    Message   => "Exception mismatch (2)");
      end;

      begin
         Key := Get_Secret_Key_Info (Seckey_List => Ref2);
         Assert (Condition => False,
                 Message   => "Exception expected (3)");

      exception
         when E : Sign_Key_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Multiple secret keys found in given keyring",
                    Message   => "Exception mismatch (3)");
      end;

      begin
         Key := Get_Secret_Key_Info (Seckey_List => Ref3);
         Assert (Condition => False,
                 Message   => "Exception expected (4)");

      exception
         when E : Sign_Key_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unsupported key type 'rsa1024'",
                    Message   => "Exception mismatch (4)");
      end;

      Key := Get_Secret_Key_Info (Seckey_List => Ref1);
      Assert (Condition => Key.Algo = RSA4096,
              Message   => "Key algorithm mismatch");
      Assert (Condition => Key.Sig_Len = GPG_Max_RSA4096_Sig_Length,
              Message   => "Signature size mismatch");
--  begin read only
   end Test_Get_Secret_Key_Info;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Crypto.Signature.GPG.Test_Data.Tests;
