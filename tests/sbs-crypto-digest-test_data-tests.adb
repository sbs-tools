--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into SBS.Crypto.Digest.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;
with System.Assertions;

--  begin read only
--  id:2.2/00/
--
--  This section can be used to add with clauses if necessary.
--
--  end read only
with Ada.Streams;

with Interfaces;

with Test_Utils;
--  begin read only
--  end read only
package body SBS.Crypto.Digest.Test_Data.Tests is

--  begin read only
--  id:2.2/01/
--
--  This section can be used to add global variables and other elements.
--
--  end read only

--  begin read only
--  end read only

--  begin read only
   procedure Test_Hash (Gnattest_T : in out Test);
   procedure Test_Hash_6e6874 (Gnattest_T : in out Test) renames Test_Hash;
--  id:2.2/6e687452fe54873d/Hash/1/0/
   procedure Test_Hash (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type SBS.Types.Hashsum_Type;
   begin
      Assert (Condition => Hash (Block => Test_Utils.Ref_Block_A)
              = Test_Utils.Ref_Block_A_Hash,
              Message   => "Hash of block A mismatch");

      declare
         use type Ada.Streams.Stream_Element;

         Altered : Types.Block_Type := Test_Utils.Ref_Block_C;
      begin
         Assert (Condition => Hash (Block => Altered)
                 = Test_Utils.Ref_Block_C_Hash,
                 Message   => "Hash of block C mismatch");

         Altered.Data (12) := Altered.Data (12) + 1;
         Assert (Condition => Hash (Block => Altered)
                 /= Test_Utils.Ref_Block_C_Hash,
                 Message   => "Hash of block C matches");
      end;
--  begin read only
   end Test_Hash;
--  end read only


--  begin read only
   procedure Test_Set_Algorithms (Gnattest_T : in out Test);
   procedure Test_Set_Algorithms_5be692 (Gnattest_T : in out Test) renames Test_Set_Algorithms;
--  id:2.2/5be69252d8dd2222/Set_Algorithms/1/0/
   procedure Test_Set_Algorithms (Gnattest_T : in out Test) is
--  end read only

      pragma Unreferenced (Gnattest_T);

      use type Interfaces.Unsigned_16;

      H : Types.Header_Type;
   begin
      Set_Algorithms (Header => H);
      Assert (Condition => H.Hash_Algorithm_ID_1 = 4,
              Message   => "Hash algo ID 1 mismatch");
      Assert (Condition => H.Hash_Algorithm_ID_2 = 0,
              Message   => "Hash algo ID 2 mismatch");
      Assert (Condition => H.Hash_Algorithm_ID_3 = 0,
              Message   => "Hash algo ID 3 mismatch");
      Assert (Condition => H.Hash_Algorithm_ID_4 = 0,
              Message   => "Hash algo ID 4 mismatch");
--  begin read only
   end Test_Set_Algorithms;
--  end read only

--  begin read only
--  id:2.2/02/
--
--  This section can be used to add elaboration code for the global state.
--
begin
--  end read only
   null;
--  begin read only
--  end read only
end SBS.Crypto.Digest.Test_Data.Tests;
