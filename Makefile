BIN_DIR = bin
OBJ_DIR = obj
DESTDIR = /usr/local

VERSION_SPEC  = src/sbs-version.ads
VERSION       = $(shell cat .version | sed 's/^v//')
GIT_REV      := $(shell git describe --always --dirty="-UNCLEAN" 2> /dev/null)

NUM_CPUS   := $(shell getconf _NPROCESSORS_ONLN)

HASHSUM_IMPL ?= sha512
SIG_IMPL     ?= gpg_rsa4096

GNAT_BUILDER_FLAGS ?= -R -j$(NUM_CPUS)
GMAKE_OPTS = -p ${GNAT_BUILDER_FLAGS} \
	$(foreach v,ADAFLAGS LDFLAGS HASHSUM_IMPL SIG_IMPL,'-X$(v)=$($(v))')

GNATTEST_EXIT   ?= on
GNATTEST_PASSED ?= show
GNATTEST_OPTS   ?= -q --test-duration --omit-sloc \
	$(foreach v,HASHSUM_IMPL SIG_IMPL,'-X$(v)=$($(v))')
GNATTEST_RUNNER  = $(OBJ_DIR)/tests/gnattest/harness/test_runner
GNATTEST_DRIVER  = $(OBJ_DIR)/tests/gnattest/harness/test_driver
TESTS_DIR        = $(CURDIR)/tests

SRC_FILES = $(wildcard src/*)

all: build_sbs

.version: FORCE
	@if [ -e .git ]; then \
		if [ -r $@ ]; then \
			if [ "$$(cat $@)" != "$(GIT_REV)" ]; then \
				echo $(GIT_REV) > $@; \
			fi; \
		else \
			echo $(GIT_REV) > $@; \
		fi \
	fi

$(VERSION_SPEC): .version
	@echo "package SBS.Version is"                  > $@
	@echo "   Version_String : constant String :=" >> $@
	@echo "     \"$(VERSION)\";"                   >> $@
	@echo "end SBS.Version;"                       >> $@

build_sbs: $(VERSION_SPEC)
	@gprbuild $(GMAKE_OPTS) -Psbs.gpr

$(OBJ_DIR)/.harness_stamp: $(SRC_FILES)
	@mkdir -p $(OBJ_DIR)/tests
	gnattest $(GNATTEST_OPTS) -Psbs_tests.gpr
	@touch $@

build_tests: $(OBJ_DIR)/.harness_stamp $(VERSION_SPEC)
	gprbuild $(GMAKE_OPTS) -P$(GNATTEST_DRIVER) -largs -fprofile-generate

tests: build_tests
	$(GNATTEST_RUNNER) --exit-status=$(GNATTEST_EXIT) --passed-tests=$(GNATTEST_PASSED)

cov: build_tests
	rm -f $(COV_DIR)/tests/*.gcda
	$(GNATTEST_RUNNER) --exit-status=off --passed-tests=$(GNATTEST_PASSED)
	lcov -c -d $(OBJ_DIR)/tests -o $(OBJ_DIR)/cov.info
	lcov -e $(OBJ_DIR)/cov.info "$(PWD)/src/*.adb" -o $(OBJ_DIR)/cov.info
	genhtml --no-branch-coverage $(OBJ_DIR)/cov.info -o $(OBJ_DIR)/cov

install: build_sbs
	install -m 2775 -d $(DESTDIR)/bin
	install $(BIN_DIR)/sbs_create $(DESTDIR)/bin
	install $(BIN_DIR)/sbs_inspect $(DESTDIR)/bin

install_tests: build_tests
	install -m 2775 -d $(DESTDIR)/tests
	install $(GNATTEST_RUNNER) $(DESTDIR)/tests
	cp -r data $(DESTDIR)/tests

prepare: $(VERSION_SPEC)

clean:
	@rm -rf $(BIN_DIR)
	@rm -rf $(OBJ_DIR)

FORCE:
