--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with System;

with SBS.Types;

package SBS.Mmap
is

   type Mode_Type is (In_File, Out_File);

   --  Memory map a file-backed region of Size bytes.
   --  Mode In_File opens the file read only.
   --  The file is overwritten if it exists and Mode is Out_File.
   procedure Mmap_File
     (Filename :     String;
      Size     :     Types.File_Size;
      Mode     :     Mode_Type;
      Address  : out System.Address);

   --  Unmap a mapped memory region.
   procedure Munmap
     (Address : System.Address;
      Size    : Types.File_Size);

   Mmap_Error : exception;

end SBS.Mmap;
