--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

package SBS.OS
is

   --  Execute given command, do not care about command output. Raises an
   --  exception if the given command fails (i.e. the executed command returns
   --  a non-zero exit status or the command is not found).
   procedure Execute (Command : String);

   --  Execute given command and return output. Raises an exception if the
   --  given command fails (i.e. the executed command returns a non-zero exit
   --  status or the command is not found).
   procedure Execute
     (Command :     String;
      Output  : out Ada.Strings.Unbounded.Unbounded_String);

   Command_Failed : exception;

end SBS.OS;
