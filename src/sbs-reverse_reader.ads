--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with SBS.Types;

package SBS.Reverse_Reader
is

   use type SBS.Types.File_Size;

   Initialized : Boolean := False;

   --  Returns remaining bytes to read.
   function Get_Bytes_To_Read return Types.File_Size
   with
      Pre => Initialized;

   --  Initialize reverse reader with given file and byte count. The requested
   --  bytes of data are packed into data blocks by calling the Get_Next_Block
   --  function. The file data is read from the end of file to the start of the
   --  file.
   procedure Init
     (Path          : String;
      Bytes_To_Read : Types.File_Size)
   with
      Pre  => not Initialized and then Bytes_To_Read > 0,
      Post => Initialized and Get_Bytes_To_Read = Bytes_To_Read;

   --  Returns initial padding bytes.
   function Get_Initial_Padding return Natural
   with
      Pre => Initialized;

   --  Return next data block.
   procedure Get_Next_Block (Item : access Types.Block_Data_Type)
   with
      Pre => Initialized and Get_Bytes_To_Read /= 0;

   --  Close file and de-initialize reader.
   procedure Close
   with
      Post => not Initialized;

   Read_Error : exception;

private

   Bytes_Remaining : Types.File_Size := Types.File_Size'Last;
   Initial_Padding : Types.Block_Padding_Type;

   function Get_Bytes_To_Read return Types.File_Size is (Bytes_Remaining);
   function Get_Initial_Padding return Natural is (Natural (Initial_Padding));

end SBS.Reverse_Reader;
