--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with System.Storage_Elements;

with Ada.Text_IO;
with Ada.Streams;
with Ada.Directories;

with Interfaces;

with SBS.Config;
with SBS.Types;
with SBS.Utils;
with SBS.Mmap;
with SBS.Reverse_Reader;
with SBS.Crypto.Digest;
with SBS.Crypto.Signature;

package body SBS.Chainer
is

   -------------------------------------------------------------------------

   procedure Run
     (Input_File  : String;
      Key_Path    : String;
      Output_File : String)
   is
      use type Types.File_Size;

      Input_Size  : constant Types.File_Size
        := Ada.Directories.Size (Name => Input_File);
      Sig_Len     : constant Positive
        := Crypto.Signature.Get_Signature_Size (Key_Path => Key_Path);
      Block_Count : Positive;
      Output_Size : Types.File_Size;
      Map_Address : System.Address;
   begin
      if Input_Size = 0 then
         raise Chainer_Error with "Input file '" & Input_File
           & "' is empty, no data to process";
      end if;

      Block_Count := Utils.Get_Block_Count (Size => Input_Size);
      Output_Size := Types.File_Size (Block_Count)
        * Config.Block_Size + Config.Header_Size + Types.File_Size (Sig_Len);

      Ada.Text_IO.Put_Line
        ("Input file size is" & Input_Size'Img
         & " bytes, requiring" & Block_Count'Img & " block(s)");
      Ada.Text_IO.Put_Line
        ("Output files size is" & Output_Size'Img & " bytes");
      Ada.Text_IO.Put_Line
        ("Maximum signature length is" & Sig_Len'Img & " bytes");

      Mmap.Mmap_File (Filename => Output_File,
                      Size     => Output_Size,
                      Mode     => Mmap.Out_File,
                      Address  => Map_Address);

      declare
         use System.Storage_Elements;

         subtype Signature_Type is Ada.Streams.Stream_Element_Array
           (1 .. Ada.Streams.Stream_Element_Offset (Sig_Len));

         Header : Types.Header_Type
           with
             Import,
             Address => Map_Address;
         Signature : Signature_Type
           with
             Import,
             Address => Map_Address + Storage_Offset (Config.Header_Size);
         Blocks : Types.Block_Array (1 .. Block_Count)
           with
             Import,
             Address => Map_Address + Storage_Offset
               (Config.Header_Size + Sig_Len);

         Hash : Types.Hashsum_Type := (others => 0);
      begin
         Header := Types.Default_Header;

         Reverse_Reader.Init (Path          => Input_File,
                              Bytes_To_Read => Input_Size);

         Ada.Text_IO.Put_Line
           ("Processing" & Block_Count'Img & " block(s)");

         for I in reverse 1 .. Block_Count loop
            Reverse_Reader.Get_Next_Block
              (Item => Blocks (I).Data'Access);

            if I /= Block_Count then
               Blocks (I).Hash_Of_Next_Block := Hash;
            else
               Blocks (I).Hash_Of_Next_Block := (others => 0);
            end if;
            Hash := Crypto.Digest.Hash (Block => Blocks (I));
         end loop;

         Header.Root_Hash := Hash;
         Crypto.Digest.Set_Algorithms (Header => Header);

         if Reverse_Reader.Get_Bytes_To_Read /= 0 then
            raise Chainer_Error with "Not all bytes read - still"
              & Reverse_Reader.Get_Bytes_To_Read'Img & " byte(s) left";
         end if;

         Header.Block_Count := Interfaces.Unsigned_32 (Block_Count);
         Header.Padding_Length := Interfaces.Unsigned_32
           (Reverse_Reader.Get_Initial_Padding);
         Header.Signature_Data_Length := Interfaces.Unsigned_32 (Sig_Len);
         Reverse_Reader.Close;

         Crypto.Signature.Set_Scheme (Header => Header);

         Ada.Text_IO.Put_Line ("Creating signature");

         declare
            Sig : constant Ada.Streams.Stream_Element_Array
              := Crypto.Signature.Sign (Key_Path => Key_Path,
                                        Header   => Header);
         begin
            Ada.Text_IO.Put_Line
              ("Created signature is" & Sig'Length'Img & " bytes");
            Signature (Sig'Range) := Sig;
         end;
      end;

      Mmap.Munmap (Address => Map_Address,
                   Size    => Output_Size);

      Ada.Text_IO.Put_Line ("Signed stream written to '" & Output_File & "'");

   exception
      when others =>
         Reverse_Reader.Close;
         raise;
   end Run;

end SBS.Chainer;
