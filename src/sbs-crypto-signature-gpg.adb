--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Fixed;

with GNAT.Regpat;

package body SBS.Crypto.Signature.GPG
is

   Sec : constant String := "sec[> ]\s{2}";

   Pattern : constant String
     := Sec & "([a-z]+[1-9][0-9]{3})\s[1-9][0-9]{3}-[0-9]{2}-[0-9]{2}\s\[SC";

   -------------------------------------------------------------------------

   function Get_Secret_Key_Info (Seckey_List : String) return Seckey_Type
   is
      use type GNAT.Regpat.Match_Location;

      Count1  : constant Natural
        := Ada.Strings.Fixed.Count (Source  => Seckey_List,
                                    Pattern => "sec   ");
      Count2  : constant Natural
        := Ada.Strings.Fixed.Count (Source  => Seckey_List,
                                    Pattern => "sec>  ");
      Count   : constant Natural := Count1 + Count2;
      Re      : constant GNAT.Regpat.Pattern_Matcher
        := GNAT.Regpat.Compile (Expression => Pattern);
      Matches : GNAT.Regpat.Match_Array (0 .. 1);
      Res     : Seckey_Type;
   begin
      if Count = 0 then
         raise Sign_Key_Error with "No secret key found in given keyring";
      elsif Count > 1 then
         raise Sign_Key_Error with
           "Multiple secret keys found in given keyring";
      end if;

      GNAT.Regpat.Match (Self    => Re,
                         Data    => Seckey_List,
                         Matches => Matches);
      if Matches (0) = GNAT.Regpat.No_Match then
         raise Sign_Key_Error with "Error extracting secret key properties";
      end if;

      declare
         Key_Type : constant String := Seckey_List
           (Matches (1).First .. Matches (1).Last);
      begin
         Res.Algo := Algorithm_Type'Value (Key_Type);

      exception
         when Constraint_Error =>
            raise Sign_Key_Error with "Unsupported key type '"
              & Key_Type & "'";
      end;

      return Res;
   end Get_Secret_Key_Info;

end SBS.Crypto.Signature.GPG;
