--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Directories;

with GNAT.OS_Lib;

package body SBS.OS
is

   -------------------------------------------------------------------------

   procedure Execute
     (Command :     String;
      Output  : out Ada.Strings.Unbounded.Unbounded_String)
   is
      use Ada.Strings.Unbounded;
      use type GNAT.OS_Lib.File_Descriptor;

      Retval   : Integer;
      Fd       : GNAT.OS_Lib.File_Descriptor;
      Tmp_Name : GNAT.OS_Lib.Temp_File_Name;
      Args     : GNAT.OS_Lib.Argument_List (1 .. 5);
   begin
      Output := Null_Unbounded_String;

      GNAT.OS_Lib.Create_Temp_File (FD   => Fd,
                                    Name => Tmp_Name);
      if Fd = GNAT.OS_Lib.Invalid_FD then
         raise Command_Failed with "Unable to create tmpfile to "
           & "capture output";
      end if;

      Args (1) := new String'("/bin/bash");
      Args (2) := new String'("-o");
      Args (3) := new String'("pipefail");
      Args (4) := new String'("-c");
      Args (5) := new String'(Command);

      GNAT.OS_Lib.Spawn
        (Program_Name           => Args (Args'First).all,
         Args                   => Args (Args'First + 1 .. Args'Last),
         Output_File_Descriptor => Fd,
         Return_Code            => Retval,
         Err_To_Out             => True);
      for A of Args loop
         GNAT.OS_Lib.Free (X => A);
      end loop;

      declare
         Del_Succ  : Boolean;
         File_Size : constant Natural
           := Natural (Ada.Directories.Size (Name => Tmp_Name));
      begin
         if File_Size > 0 then
            declare
               Buffer     : String (1 .. File_Size);
               Bytes_Read : Integer := 0;
            begin
               GNAT.OS_Lib.Lseek
                 (FD     => Fd,
                  offset => 0,
                  origin => GNAT.OS_Lib.Seek_Set);
               Bytes_Read := GNAT.OS_Lib.Read
                 (FD => Fd,
                  A  => Buffer'Address,
                  N  => File_Size);
               GNAT.OS_Lib.Close (FD => Fd);
               GNAT.OS_Lib.Delete_File (Name    => Tmp_Name,
                                        Success => Del_Succ);
               if not Del_Succ then
                  raise Command_Failed with "Unable to remove temporary file '"
                    & Tmp_Name & "'";
               end if;
               if Bytes_Read /= File_Size then
                  raise Command_Failed with
                    "Incomplete read of command output: expected"
                    & File_Size'Img & " byte(s), only read" & Bytes_Read'Img;
               end if;

               Output := To_Unbounded_String (Buffer);
            end;
         else
            GNAT.OS_Lib.Close (FD => Fd);
            GNAT.OS_Lib.Delete_File (Name    => Tmp_Name,
                                     Success => Del_Succ);
         end if;
      end;

      if Retval /= 0 then
         raise Command_Failed with "Execution of command '"
           & Command & "' failed";
      end if;
   end Execute;

   -------------------------------------------------------------------------

   procedure Execute (Command : String)
   is
      Dummy : Ada.Strings.Unbounded.Unbounded_String;
   begin
      Execute (Command => Command,
               Output  => Dummy);
   end Execute;

end SBS.OS;
