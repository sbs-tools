--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Direct_IO;
with Ada.Strings.Unbounded;

with SBS.OS;

package body SBS.Utils
is

   -------------------------------------------------------------------------

   function Create_Tempdir return String
   is
      Tmp_Dir : Ada.Strings.Unbounded.Unbounded_String;
   begin
      OS.Execute (Command => "mktemp -d",
                  Output  => Tmp_Dir);

      declare
         Str : constant String := Ada.Strings.Unbounded.To_String (Tmp_Dir);
      begin
         return Str (Str'First .. Str'Last - 1);
      end;
   end Create_Tempdir;

   -------------------------------------------------------------------------

   function Hex_To_Bytes
     (Str : String)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;

      Result : Stream_Element_Array
        (1 .. Stream_Element_Offset ((Str'Length + 1) / 2))
        := (others => 0);
   begin
      for Index in Result'Range loop
         declare
            Hex_Byte : String (1 .. 2) := "00";
         begin
            Hex_Byte (1) := Str (Str'First - 1 + (Positive (Index) * 2 - 1));
            if Positive (Index * 2) <= Str'Length then
               Hex_Byte (2) := Str (Str'First - 1 + (Positive (Index) * 2));
            end if;
            Result (Index) := Stream_Element'Value
              ("16#" & Hex_Byte & "#");
         end;
         exit when Positive (Index) * 2 - 1 > Str'Length;
      end loop;

      return Result;

   exception
      when Constraint_Error =>
         raise Conversion_Error with "'" & Str & "' is not a valid hex"
           & " string";
   end Hex_To_Bytes;

   -------------------------------------------------------------------------

   function To_Hex_Str (A : Ada.Streams.Stream_Element_Array) return String
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      --  Convert given stream element to hex.
      function Hex (E : Stream_Element) return String;

      ----------------------------------------------------------------------

      function Hex (E : Stream_Element) return String
      is
         Hex_Chars : constant array (Stream_Element range 0 .. 15)
           of Character
             := ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f');
         Half_Byte_1 : constant Stream_Element := E mod 16;
         Half_Byte_2 : constant Stream_Element := E / 16;
      begin
         return Hex_Chars (Half_Byte_2) & Hex_Chars (Half_Byte_1);
      end Hex;

      Res : Unbounded_String;
   begin
      for Element of A loop
         Res := Res & Hex (E => Element);
      end loop;

      return To_String (Res);
   end To_Hex_Str;

   -------------------------------------------------------------------------

   procedure Write_Hdr
     (Hdr  : Types.Header_Type;
      Path : String)
   is
      package Hdr_IO is new Ada.Direct_IO
        (Element_Type => Types.Header_Type);

      Fd : Hdr_IO.File_Type;
   begin
      Hdr_IO.Create
        (File => Fd,
         Mode => Hdr_IO.Out_File,
         Name => Path);
      Hdr_IO.Write
        (File => Fd,
         Item => Hdr);
      Hdr_IO.Close (File => Fd);
   end Write_Hdr;

end SBS.Utils;
