--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Directories;

with SBS.Config;
with SBS.Types;

package SBS.Utils
is

   use type Ada.Directories.File_Size;

   --  Return number of blocks required to store file of given size.
   function Get_Block_Count (Size : Ada.Directories.File_Size) return Positive
   with
      Pre => Size / Config.Block_Data_Size + 1 <= Ada.Directories.File_Size
                (Positive'Last);

   --  Convert given stream element array to hex string.
   function To_Hex_Str (A : Ada.Streams.Stream_Element_Array) return String
   with
      Pre => A'Length > 0;

   --  Return byte sequence of hex string.
   function Hex_To_Bytes
     (Str : String)
      return Ada.Streams.Stream_Element_Array
   with
      Pre => Str'Length > 0;

   --  Write header to given path.
   procedure Write_Hdr
     (Hdr  : Types.Header_Type;
      Path : String);

   --  Create temp directory (uses /bin/mktemp internally).
   function Create_Tempdir return String;

   Conversion_Error : exception;

private

   function Get_Block_Count (Size : Ada.Directories.File_Size) return Positive
   is (Positive
       ((Size + Ada.Directories.File_Size (Config.Block_Data_Size) - 1)
        / Ada.Directories.File_Size (Config.Block_Data_Size)));

end SBS.Utils;
