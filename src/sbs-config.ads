--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package SBS.Config
is

   Version_Magic  : constant := 16#e6019598#;

   type Hash_Algorithm_Type is
     (Hash_None,
      Hash_SHA1,
      Hash_SHA2_256,
      Hash_SHA2_384,
      Hash_SHA2_512,
      Hash_RIPEMD_160);

   type Signature_Scheme_Type is
     (Signature_PGP);

   Block_Size      : constant := 64 * 1024;
   Header_Size     : constant := 100;
   Hashsum_Length  : constant := 64;
   Block_Data_Size : constant := Block_Size - Hashsum_Length;

private

   for Hash_Algorithm_Type use
     (Hash_None       => 0,
      Hash_SHA1       => 1,
      Hash_SHA2_256   => 2,
      Hash_SHA2_384   => 3,
      Hash_SHA2_512   => 4,
      Hash_RIPEMD_160 => 5);

   for Signature_Scheme_Type use
     (Signature_PGP => 1);

end SBS.Config;
