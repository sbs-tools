--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with SBS.Types;
with SBS.Config;

package SBS.Stream_Verifier
is

   type Verifier_Type is private;

   --  Initialize hash verifier with given root hash and expected block count.
   procedure Init
     (Verifier    : out Verifier_Type;
      Root_Hash   :     Types.Hashsum_Type;
      Block_Count :     Positive);

   --  Update buffer state of verifier with given data. Returns False if a
   --  hash mismatch has been detected.
   procedure Update
     (Verifier : in out Verifier_Type;
      Data     :        Ada.Streams.Stream_Element_Array;
      Success  :    out Boolean);

   --  Finalize hash verifier. Call this procedure to ensure that all blocks
   --  have been checked.
   procedure Finalize
     (Verifier :     Verifier_Type;
      Success  : out Boolean);

private

   use type Ada.Streams.Stream_Element_Offset;

   subtype Buffer_Len is Ada.Streams.Stream_Element_Offset range
     0 .. Config.Block_Size;
   subtype Buffer_Range is Buffer_Len range 1 .. Buffer_Len'Last;

   type Verifier_Type is record
      Pos            : Buffer_Range;
      Block_Count    : Natural := 0;  --  Used to detect non-initialized state
      Blocks_Checked : Long_Long_Integer;
      Next_Hash      : Types.Hashsum_Type;
      Data           : Ada.Streams.Stream_Element_Array
        (1 .. Config.Block_Size);
   end record;

   Null_Verifier : constant Verifier_Type
     := (Pos            => Buffer_Range'First,
         Block_Count    => 0,
         Blocks_Checked => 0,
         Next_Hash      => (others => 0),
         Data           => (others => 0));

end SBS.Stream_Verifier;
