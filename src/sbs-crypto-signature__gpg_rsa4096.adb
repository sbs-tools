--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Directories;
with Ada.Strings.Unbounded;
with Ada.Streams.Stream_IO;

with SBS.OS;
with SBS.Config;
with SBS.Utils;
with SBS.Crypto.Signature.GPG;

package body SBS.Crypto.Signature
is

   use Ada.Strings.Unbounded;

   -------------------------------------------------------------------------

   function Get_Signature_Size (Key_Path : String) return Positive
   is
      Key_Listing : Unbounded_String;
   begin
      OS.Execute (Command => "gpg --batch --homedir "
                  & Key_Path & " --list-secret-keys",
                  Output  => Key_Listing);
      return GPG.Get_Secret_Key_Info
        (Seckey_List => To_String (Key_Listing)).Sig_Len;
   end Get_Signature_Size;

   -------------------------------------------------------------------------

   procedure Set_Scheme (Header : in out Types.Header_Type)
   is
   begin
      Header.Signature_Scheme_ID := Config.Signature_Scheme_Type'Enum_Rep
        (Config.Signature_PGP);
   end Set_Scheme;

   -------------------------------------------------------------------------

   function Sign
     (Key_Path : String;
      Header   : Types.Header_Type)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;

      Cmd_Out : Unbounded_String;
      Tmpdir  : constant String := Utils.Create_Tempdir;
      Tmpfile : constant String := Tmpdir & "/to_sign";
   begin
      Utils.Write_Hdr (Hdr  => Header,
                       Path => Tmpfile);

      begin
         OS.Execute
           (Command => "gpg --digest-algo SHA512 --batch --homedir "
            & Key_Path & " -b " & Tmpfile,
            Output  => Cmd_Out);

      exception
         when E : OS.Command_Failed =>
            Ada.Text_IO.Put_Line
              ("ERROR: gpg signature failed, error output:");
            Ada.Text_IO.Put_Line (To_String (Cmd_Out));
            raise Sign_Error with Ada.Exceptions.Exception_Message (X => E);
      end;

      declare
         use type Ada.Directories.File_Size;

         Sig_Fd   : Stream_IO.File_Type;
         Sig_Size : Ada.Directories.File_Size;
         Sig_Name : constant String := Tmpfile & ".sig";
      begin
         Sig_Size := Ada.Directories.Size (Name => Sig_Name);

         if Sig_Size = 0 then
            raise Sign_IO_Error with "Signature file '" & Sig_Name
              & "' is empty";
         end if;

         Stream_IO.Open
           (File => Sig_Fd,
            Mode => Stream_IO.In_File,
            Name => Sig_Name);

         declare
            Last : Stream_Element_Offset;
            Res  : Stream_Element_Array
              (1 .. Stream_Element_Offset (Sig_Size));
         begin
            Stream_IO.Read (File => Sig_Fd,
                            Item => Res,
                            Last => Last);
            Stream_IO.Close (File => Sig_Fd);
            if Last /= Stream_Element_Offset (Sig_Size) then
               raise Sign_IO_Error with "Incomplete read of .sig data";
            end if;

            Ada.Directories.Delete_Tree (Directory => Tmpdir);

            return Res;
         end;
      end;

   exception
      when others =>
         if Ada.Directories.Exists (Name => Tmpdir) then
            Ada.Directories.Delete_Tree (Directory => Tmpdir);
         end if;
         raise;
   end Sign;

end SBS.Crypto.Signature;
