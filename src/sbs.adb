--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;
with Ada.Directories;

with SBS.Chainer;

package body SBS
is

   -------------------------------------------------------------------------

   procedure Process
     (Input_File  : String;
      Key_Path    : String;
      Output_File : String)
   is
   begin
      Ada.Text_IO.Put_Line ("Processing input file '" & Input_File & "'");

      if not Ada.Directories.Exists (Name => Input_File) then
         raise Processing_Error with "Input file '" & Input_File
           & "' does not exist";
      end if;

      if not Ada.Directories.Exists (Name => Key_Path) then
         raise Processing_Error with "Key path '" & Key_Path
           & "' does not exist";
      end if;

      Chainer.Run (Input_File  => Input_File,
                   Key_Path    => Key_Path,
                   Output_File => Output_File);
   end Process;

end SBS;
