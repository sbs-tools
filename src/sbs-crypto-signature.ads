--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;

with SBS.Types;

package SBS.Crypto.Signature
is

   --  Sign given header with specified key.
   function Sign
     (Key_Path : String;
      Header   : Types.Header_Type)
      return Ada.Streams.Stream_Element_Array;

   --  Return size of signature in bytes for given signature key path.
   function Get_Signature_Size (Key_Path : String) return Positive;

   --  Set signature scheme configuration.
   procedure Set_Scheme (Header : in out Types.Header_Type);

   Sign_Error, Sign_Key_Error, Sign_IO_Error : exception;

end SBS.Crypto.Signature;
