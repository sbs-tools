--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams;
with Ada.Directories;

with Interfaces;

with SBS.Config;

package SBS.Types
is

   subtype File_Size is Ada.Directories.File_Size;

   subtype Hashsum_Type is Ada.Streams.Stream_Element_Array
     (1 .. Config.Hashsum_Length);

   subtype Block_Data_Range is Ada.Streams.Stream_Element_Offset range
     1 .. Config.Block_Data_Size;

   subtype Block_Padding_Type is Natural range
     0 .. Positive (Block_Data_Range'Last);

   subtype Block_Data_Type is Ada.Streams.Stream_Element_Array
     (Block_Data_Range);

   type Header_Type is record
      Version_Magic         : Interfaces.Unsigned_32;
      Block_Count           : Interfaces.Unsigned_32;
      Block_Size            : Interfaces.Unsigned_32;
      Signature_Data_Length : Interfaces.Unsigned_32;
      Header_Size           : Interfaces.Unsigned_16;
      Hashsum_Length        : Interfaces.Unsigned_16;
      Hash_Algorithm_ID_1   : Interfaces.Unsigned_16;
      Hash_Algorithm_ID_2   : Interfaces.Unsigned_16;
      Hash_Algorithm_ID_3   : Interfaces.Unsigned_16;
      Hash_Algorithm_ID_4   : Interfaces.Unsigned_16;
      Signature_Scheme_ID   : Interfaces.Unsigned_16;
      Reserved              : Interfaces.Unsigned_16;
      Padding_Length        : Interfaces.Unsigned_32;
      Root_Hash             : Hashsum_Type;
   end record
     with
       Size => Config.Header_Size * 8;

   Default_Header : constant Header_Type;

   type Block_Type is record
      Hash_Of_Next_Block : Hashsum_Type;
      Data               : aliased Block_Data_Type;
   end record
     with
       Size => Config.Block_Size * 8;

   type Block_Array is array (Positive range <>) of Block_Type
     with
       Pack,
       Component_Size => Config.Block_Size * 8;

private

   for Header_Type use record
      Version_Magic         at  0 range 0 .. 31;
      Block_Count           at  4 range 0 .. 31;
      Block_Size            at  8 range 0 .. 31;
      Signature_Data_Length at 12 range 0 .. 31;
      Header_Size           at 16 range 0 .. 15;
      Hashsum_Length        at 18 range 0 .. 15;
      Hash_Algorithm_ID_1   at 20 range 0 .. 15;
      Hash_Algorithm_ID_2   at 22 range 0 .. 15;
      Hash_Algorithm_ID_3   at 24 range 0 .. 15;
      Hash_Algorithm_ID_4   at 26 range 0 .. 15;
      Signature_Scheme_ID   at 28 range 0 .. 15;
      Reserved              at 30 range 0 .. 15;
      Padding_Length        at 32 range 0 .. 31;
      Root_Hash             at 36 range 0 .. Config.Hashsum_Length * 8 - 1;
   end record;

   Default_Header : constant Header_Type
     := (Version_Magic         => Config.Version_Magic,
         Block_Count           => 0,
         Block_Size            => Config.Block_Size,
         Signature_Data_Length => 0,
         Header_Size           => Config.Header_Size,
         Hashsum_Length        => Config.Hashsum_Length,
         Reserved              => 0,
         Padding_Length        => 0,
         Root_Hash             => (others => 0),
         others                => 0);

   for Block_Type use record
      Hash_Of_Next_Block at 0 range 0 .. Config.Hashsum_Length * 8 - 1;
      Data               at Config.Hashsum_Length range
        0 .. Config.Block_Data_Size * 8 - 1;
   end record;

end SBS.Types;
