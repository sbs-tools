--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams.Stream_IO;

with SBS.Config;

package body SBS.Reverse_Reader
is

   use Ada.Streams;

   Fd : Stream_IO.File_Type;

   -------------------------------------------------------------------------

   procedure Init
     (Path          : String;
      Bytes_To_Read : Types.File_Size)
   is
   begin
      Stream_IO.Open (File => Fd,
                      Mode => Stream_IO.In_File,
                      Name => Path);

      Bytes_Remaining := Bytes_To_Read;
      if Bytes_To_Read mod Config.Block_Data_Size /= 0 then
         Initial_Padding := Config.Block_Data_Size -
           Natural (Bytes_To_Read mod Config.Block_Data_Size);
      else
         Initial_Padding := 0;
      end if;
      Initialized := True;
   end Init;

   -------------------------------------------------------------------------

   procedure Get_Next_Block (Item : access Types.Block_Data_Type)
   is
      Full_Block : constant Boolean
        := Bytes_Remaining >= Item.all'Length;
      File_Idx   : Types.File_Size;
      Last       : Stream_Element_Offset;
   begin
      if Full_Block then
         File_Idx := Bytes_Remaining - Item.all'Length + 1;

         Stream_IO.Read (File => Fd,
                         Item => Item.all,
                         Last => Last,
                         From => Stream_IO.Positive_Count (File_Idx));
         if Last /= Item.all'Last then
            raise Read_Error with "Unable to read complete block of"
              & Item.all'Length'Img & " bytes, only" &  Last'Img
              & " read";
         end if;
      else
         declare
            Len  : constant Stream_Element_Offset
              := Stream_Element_Offset
                (Item.all'Length - Initial_Padding);
            Data : Stream_Element_Array (1 .. Len);
         begin
            File_Idx := 1;

            Stream_IO.Read (File => Fd,
                            Item => Data,
                            Last => Last,
                            From => Stream_IO.Positive_Count (File_Idx));
            if Last /= Len then
               raise Read_Error with "Unable to read partial block of"
                 & Len'Img & " byte(s), only" &  Last'Img & " read";
            end if;

            Item.all := (others => 0);
            Item.all (Item.all'Last - Len + 1 .. Item.all'Last) := Data;
         end;
      end if;

      Bytes_Remaining := File_Idx - 1;
   end Get_Next_Block;

   -------------------------------------------------------------------------

   procedure Close
   is
   begin
      if Stream_IO.Is_Open (File => Fd) then
         Stream_IO.Close (File => Fd);
      end if;
      Initialized := False;
   end Close;

end SBS.Reverse_Reader;
