--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with SBS.Crypto.Digest;

package body SBS.Stream_Verifier
is

   --  Check hash of current block buffer data.
   procedure Check_Hash
     (Verifier : in out Verifier_Type;
      Success  :    out Boolean);

   -------------------------------------------------------------------------

   procedure Check_Hash
     (Verifier : in out Verifier_Type;
      Success  :    out Boolean)
   is
      use type Ada.Streams.Stream_Element_Array;

      Block : Types.Block_Type
        with
          Address => Verifier.Data'Address;
   begin
      Success := Verifier.Next_Hash = Crypto.Digest.Hash (Block => Block);
      if Success then
         Verifier.Next_Hash := Block.Hash_Of_Next_Block;
      end if;
   end Check_Hash;

   -------------------------------------------------------------------------

   procedure Init
     (Verifier    : out Verifier_Type;
      Root_Hash   :     Types.Hashsum_Type;
      Block_Count :     Positive)
   is
   begin
      Verifier := Null_Verifier;

      Verifier.Next_Hash   := Root_Hash;
      Verifier.Block_Count := Block_Count;
   end Init;

   -------------------------------------------------------------------------

   procedure Finalize
     (Verifier :     Verifier_Type;
      Success  : out Boolean)
   is
   begin
      Success := Verifier.Block_Count /= 0 and then
        Long_Long_Integer (Verifier.Block_Count) = Verifier.Blocks_Checked;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Update
     (Verifier : in out Verifier_Type;
      Data     :        Ada.Streams.Stream_Element_Array;
      Success  :    out Boolean)
   is
      Data_Idx : Ada.Streams.Stream_Element_Offset := Data'First;
   begin
      Success := True;

      if Data'Length = 0
        or else Verifier.Block_Count = 0
        or else Verifier.Blocks_Checked > Long_Long_Integer
          (Verifier.Block_Count)
      then
         Success := False;
         return;
      end if;

      while Data'Last - Data_Idx >= 0
        and then Verifier.Blocks_Checked <= Long_Long_Integer
          (Verifier.Block_Count)
      loop
         declare
            Buffer_Last : constant Buffer_Len
              := Verifier.Data'Last - Verifier.Pos;
            Data_Last   : constant Ada.Streams.Stream_Element_Offset
              := Data'Last - Data_Idx;
            Length      : constant Buffer_Len
              := Buffer_Range'Min (Buffer_Last, Data_Last);
         begin
            Verifier.Data (Verifier.Pos .. Verifier.Pos + Length)
              := Data (Data_Idx .. Data_Idx + Length);

            Verifier.Pos := Verifier.Pos + Length;
            if Verifier.Pos = Verifier.Data'Last then
               Check_Hash
                 (Verifier => Verifier,
                  Success  => Success);
               exit when not Success;

               Verifier.Blocks_Checked := Verifier.Blocks_Checked + 1;

               Verifier.Pos  := Verifier.Data'First;
               Verifier.Data := (others => 0);
            else
               Verifier.Pos := Verifier.Pos + 1;
            end if;

            Data_Idx := Data_Idx + Length + 1;
         end;
      end loop;
   end Update;

end SBS.Stream_Verifier;
