--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNAT.OS_Lib;

with Interfaces.C.Strings;

with System.Storage_Elements;

package body SBS.Mmap
is

   use type Interfaces.C.int;

   type off_t  is new Interfaces.Integer_64;
   type Bits   is new Interfaces.Unsigned_32;
   type mode_t is new Interfaces.Unsigned_32;

   -------------------------------------------------------------------------

   O_RDONLY   : constant Bits := 0;
   O_RDWR     : constant Bits := 2;
   O_CREAT    : constant Bits := 64;
   MAP_SHARED : constant Bits := 1;
   PROT_READ  : constant Bits := 1;
   PROT_WRITE : constant Bits := 2;
   MAP_FAILED : constant System.Address
     := System'To_Address (System.Storage_Elements.Integer_Address'Last);

   -------------------------------------------------------------------------

   function C_open
     (path  : Interfaces.C.Strings.chars_ptr;
      oflag : Interfaces.C.int;
      mode  : mode_t)
      return Interfaces.C.int
     with
       Import,
       Convention => C,
       Link_Name  => "open";

   function C_close
     (fd : Interfaces.C.int)
      return Interfaces.C.int
     with
       Import,
       Convention => C,
       Link_Name  => "close";

   function C_mmap
     (addr   : System.Address;
      len    : Interfaces.C.size_t;
      prot   : Interfaces.C.int;
      flags  : Interfaces.C.int;
      fildes : Interfaces.C.int;
      off    : off_t)
      return System.Address
     with
       Import,
       Convention => C,
       Link_Name  => "mmap";

   function C_munmap
     (addr : System.Address;
      len  : Interfaces.C.size_t)
      return Interfaces.C.int
     with
       Import,
       Convention => C,
       Link_Name  => "munmap";

   function C_ftruncate
     (fd     : Interfaces.C.int;
      length : off_t)
      return Interfaces.C.int
     with
       Import,
       Convention => C,
       Link_Name  => "ftruncate";

   -------------------------------------------------------------------------

   procedure Mmap_File
     (Filename :     String;
      Size     :     Types.File_Size;
      Mode     :     Mode_Type;
      Address  : out System.Address)
   is
      use type System.Address;

      File_Descriptor : Interfaces.C.int;
      Return_Value    : Interfaces.C.int;
      Filename_C      : aliased Interfaces.C.char_array
        := Interfaces.C.To_C (Item => Filename);
      Filename_Ptr    : constant Interfaces.C.Strings.chars_ptr
        := Interfaces.C.Strings.To_Chars_Ptr (Filename_C'Unchecked_Access);
      Oflags_C        : constant Interfaces.C.int
        := Interfaces.C.int (if Mode = In_File then O_RDONLY
                             else (O_RDWR or O_CREAT));
      Prot_C          : constant Interfaces.C.int
        := Interfaces.C.int (if Mode = In_File then PROT_READ else PROT_WRITE);
   begin
      File_Descriptor := C_open
        (path  => Filename_Ptr,
         oflag => Oflags_C,
         mode  => 8#640#);

      if File_Descriptor < 0 then
         raise Mmap_Error with "Error opening file " & Filename;
      end if;

      if Mode = Out_File then
         Return_Value := C_ftruncate
           (fd     => File_Descriptor,
            length => off_t (Size));

         if Return_Value < 0 then
            raise Mmap_Error with "Error in ftruncate - "
              & GNAT.OS_Lib.Errno_Message;
         end if;
      end if;

      Address := C_mmap
        (addr   => System.Null_Address,
         len    => Interfaces.C.size_t (Size),
         prot   => Prot_C,
         flags  => Interfaces.C.int (MAP_SHARED),
         fildes => File_Descriptor,
         off    => 0);

      if Address = MAP_FAILED then
         raise Mmap_Error with "Mmap failed - " & GNAT.OS_Lib.Errno_Message;
      end if;

      Return_Value := C_close (fd => File_Descriptor);

      if Return_Value < 0 then
         raise Mmap_Error
           with "Error in close (" & Return_Value'Img & ")";
      end if;
   end Mmap_File;

   -------------------------------------------------------------------------

   procedure Munmap
     (Address : System.Address;
      Size    : Types.File_Size)
   is
      Return_Value : Interfaces.C.int;
   begin
      Return_Value := C_munmap
        (addr => Address,
         len  => Interfaces.C.size_t (Size));

      if Return_Value < 0 then
         raise Mmap_Error with "Munmap failed - " & GNAT.OS_Lib.Errno_Message;
      end if;
   end Munmap;

end SBS.Mmap;
