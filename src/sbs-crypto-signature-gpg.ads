--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package SBS.Crypto.Signature.GPG
is

   type Algorithm_Type is (RSA4096);

   --  GPG detached signature packet header (RFC 4880, section 5.2) is assumed
   --  to be 54 bytes, resulting in the following maximum signature lengths for
   --  specific key sizes.
   --
   --  RSA4096: (4096 + (54 * 8)) / 8
   --
   --  NOTE: The data part in the signature packet is not always the full 4096
   --        bits, that's why the resulting signature may be smaller (e.g. 565
   --        bytes).
   GPG_Max_RSA4096_Sig_Length : constant := 566;

   type Seckey_Type is record
      Algo    : Algorithm_Type;
      Sig_Len : Positive := GPG_Max_RSA4096_Sig_Length;
   end record;

   --  Extract secret key information from given secret key listing. Raises
   --  an exception if either multiple secret keys are found or the keyring
   --  contains a key with unsupported algorithm / key size.
   function Get_Secret_Key_Info (Seckey_List : String) return Seckey_Type;

end SBS.Crypto.Signature.GPG;
