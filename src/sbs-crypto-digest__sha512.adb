--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2020  codelabs GmbH
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNAT.SHA512;

with SBS.Config;

package body SBS.Crypto.Digest
is

   -------------------------------------------------------------------------

   function Hash (Block : Types.Block_Type) return Types.Hashsum_Type
   is
      Ctx : GNAT.SHA512.Context := GNAT.SHA512.Initial_Context;
   begin
      GNAT.SHA512.Update (C     => Ctx,
                          Input => Block.Hash_Of_Next_Block);
      GNAT.SHA512.Update (C     => Ctx,
                          Input => Block.Data);
      return GNAT.SHA512.Digest (C => Ctx);
   end Hash;

   -------------------------------------------------------------------------

   procedure Set_Algorithms (Header : in out Types.Header_Type)
   is
   begin
      Header.Hash_Algorithm_ID_1 := Config.Hash_Algorithm_Type'Enum_Rep
        (Config.Hash_SHA2_512);
      Header.Hash_Algorithm_ID_2 := Config.Hash_Algorithm_Type'Enum_Rep
        (Config.Hash_None);
      Header.Hash_Algorithm_ID_3 := Config.Hash_Algorithm_Type'Enum_Rep
        (Config.Hash_None);
      Header.Hash_Algorithm_ID_4 := Config.Hash_Algorithm_Type'Enum_Rep
        (Config.Hash_None);
   end Set_Algorithms;

end SBS.Crypto.Digest;
